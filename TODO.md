add starting rules for the extra elements as well





# TODO
* Finish recombination rule processing
  * i think this is done
* implement random advancment of an A Chem
  * i think this is basically done - just need to do the StdGen -> proccing either rule, drain,sink, etc
* evaluation of object similarity and other evaluation stuff
  * thinka bout how variance would tie in to evaluation
  * evaluate based on variance of similiarity as well
  * variance as in stat term - ms stott suggestion


* RR RULE PROCESSING
  * check for list reversals - requires muyltiple wildcards in a RR
    * i think this is done


* RR Generating
  * Histogram of appearance of unique ones
  * try to do a thing where perfect ones are removed from pop as time goes on, make seperate pool or something
    * whenever we detect things with score of 1, we pop all elements with score of 1. Then pop the next 200 most fit ones? play with value. need to keep the molecules in that rule around, dont want them to go extinct, but also don't want to keep too many of them around.
    * try doing a thing where you dont even combine - just mutate
      * should be better tbh
      * UPDATE ON THIS: its not, since the generation can be initially too unbalanced to ever match.
      * rewrite entire recombination rule generation to be conserving from the start
        * work by generating a list of patternelements
        * make a copy, shuffle
        * make a thing that splits up a list of patternelements into various subchunks for objects
        * from objects do same thing, make lines, add wildcard endings
        * make a mutateSG for balancing the line/object wildcards
        * should work better
        * once better recombination rule genertion is done, make sure the artificial chemistry generator works
        * make a create artificial chemistry by pulling a random number of recombination rules from a MASSIVE pool
          * like 10000 at least probably - store this in a file
        * remember that the generator only needs to make a couple for artificial chemistries. new ones can be made by reseeding the massive pool
  * mess around with ratios of keep, combine, and pad


# TURTLE
* look into turtle - for adding git commit number to csv files (need to run git commands and stuff for this)
  * REALLY EXTRA THOUGH
    * try to actually generate some decent artificial chemistries first
* also would allow for auto labeling the csv file based on function ran
  * also could read file to get proper increment - number will persist between runs, means i dont have to manually increment each time
    * also could make it a commandline argument

# EMAIL PEOPLE FOR HELP ABOUT SEARCH GENERATION
* the authors of the ludi paper probably
