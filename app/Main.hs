module Main where
    
import SearchGenerator.RecombinationRule
import ArtificialChemistry.RandomRR
import SearchGenerator.ArtificialChemistry
import ArtificialChemistry.Data hiding (main)
import System.Random
import ArtificialChemistry.Evaluate
import ArtificialChemistry.ACGen

main :: IO ()
main = do
    putStrLn $ show $ testACGenExact 125
    gdfExport
    {-g <- newStdGen
    let b = createACGen tokenListRestricted g
    let m = (iterate addRuleACGen b) !! 1000
    print m-}

    



--main = generatePerfect "goodRRs"
--main for testing creation of AC, and the combination
{-main = do
    a <- newStdGen >>= (\x -> return (createAC goodRRsSize goodRRs x))
    --b <- newStdGen >>= (\x -> return (createAC 645891 goodRRs x))
    --print $ length a
    --print a
    print $ compareTokensRules tokenList a
    --print $ compareTokensRules tokenList b
    --g <- newStdGen
    --let c = combineAC g a b
    --print $ compareTokensRules tokenList c
-}
--main = runAC
    
    
