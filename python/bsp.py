import tcod.bsp
import random
import time

seed = time.time()

random.seed(seed)
bsp = tcod.bsp.BSP(x=0, y=0, width=64, height=48)
bsp.split_recursive(
    depth=5,
    min_width=7,
    min_height=7,
    max_horizontal_ratio=1.5,
    max_vertical_ratio=.5,
    seed=tcod.random.Random(tcod.random.MERSENNE_TWISTER, seed).random_c,
)

# In pre order, leaf nodes are visited before the nodes that connect them.

def main():
    global seed, bsp
    screen_width = 64
    screen_height = 48

    #tcod.console_set_custom_font('rogue/fonts/tileset.png', tcod.FONT_TYPE_GREYSCALE | tcod.FONT_LAYOUT_TCOD)
    tcod.console_set_custom_font('rogue/fonts/tileset.png', tcod.FONT_LAYOUT_ASCII_INROW)


    tcod.console_init_root(screen_width, screen_height, 'tcod tutorial revised', False)

    con = tcod.console_new(screen_width, screen_height)
    cor = tcod.console_new(screen_width, screen_height)



    key = tcod.Key()
    mouse = tcod.Mouse()
    tick = 1
    dab = False
    while not tcod.console_is_window_closed():
        random.seed(seed)
        tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS, key, mouse)
        if (key.text =='z'):
            exit()
        if (key.text =='p'):
            tcod.sys_save_screenshot()
        if (key.text == 'd'):
            dab = not dab
            if dab:
                tick = tick + 1
        if (key.text == 'a'):
            dab = False
            tick = tick - 1
        if (key.text == 'q'):
            con.clear()
            cor.clear()
            tick = 1
            dab = False
            seed = time.time()

            random.seed(seed)
            bsp = tcod.bsp.BSP(x=0, y=0, width=64, height=48)
            bsp.split_recursive(
                depth=5,
                min_width=7,
                min_height=7,
                max_horizontal_ratio=1.5,
                max_vertical_ratio=.5,
                seed=tcod.random.Random(tcod.random.MERSENNE_TWISTER, seed).random_c,
            )

        tcod.console_set_default_foreground(cor, tcod.gray)
        tcod.console_set_default_foreground(con, tcod.white)
        #con.clear()
        if (tick > 7):
            con.clear()
        random.seed(seed)
        for node in bsp.post_order():
            if (node.level == 5):
                tcod.console_set_default_foreground(con, tcod.white)
            if (node.level == 4):
                tcod.console_set_default_foreground(con, tcod.green)
            if (node.level == 3):
                tcod.console_set_default_foreground(con, tcod.white)
            if (node.level == 2):
                tcod.console_set_default_foreground(con, tcod.green)
            if (node.level == 1):
                tcod.console_set_default_foreground(con, tcod.white)

            if tick > 8:
                tcod.console_set_default_foreground(con, tcod.gray)
                asd = tcod.BKGND_ADD
                if node.children:
                    node1, node2 = node.children
                    if (node1.flag and node2.flag):
                        #map[node1.xs + node1.widthX//2,node1.ys + node1.heightY//2] = 8
                        #map[node2.xs + node2.widthX//2,node2.ys + node2.heightY//2] = 8
                        node.flag = True
                        node.xs = node2.xs
                        node.ys = node2.ys
                        node.widthX = node2.widthX
                        node.heightY = node2.heightY

                        start = [node1.xs+int(random.uniform(.25, .75)*node1.widthX), node1.ys+int(random.uniform(.25,.75)*node1.heightY)]
                        end = [node2.xs+int(random.uniform(.25,.75)*node2.widthX), node2.ys+int(random.uniform(.25,.75)*node2.heightY)]
                        if (abs(end[1]-start[1]) > abs(end[0]-start[0])):
                            cutoff = int(random.uniform(end[1], start[1]))
                            if (cutoff in [node1.ys, node1.ys+node1.heightY, node2.ys, node2.ys+node2.heightY]):
                                cutoff = cutoff - 1
                            for i in range(end[1], start[1], 1 if (end[1]<start[1]) else -1):
                                if (i < cutoff):
                                    #map[start[0],i]=8
                                    cor.put_char(start[0], i, 178, asd)
                                elif (i == cutoff):
                                    for j in range(end[0], start[0], 1 if (end[0]<start[0]) else -1):
                                        #map[j,i] = 8
                                        cor.put_char(j, i, 178, asd)
                                    cor.put_char(start[0], i, 178, asd)
                                else:
                                    #map[end[0], i]=8
                                    cor.put_char(end[0], i, 178,asd)
                        else:
                            cutoff = int(random.uniform(end[0], start[0]))
                            if (cutoff in [node1.xs, node1.xs+node1.widthX, node2.xs, node2.xs+node2.widthX]):
                                cutoff = cutoff - 1
                            for i in range(end[0], start[0], 1 if (end[0]<start[0]) else -1):
                                if (i < cutoff):
                                    cor.put_char(i, start[1], 178,asd)
                                elif (i == cutoff):
                                    for j in range(end[1], start[1], 1 if (end[1]<start[1]) else -1):
                                        cor.put_char(i, j, 178,asd)
                                    cor.put_char(i, start[1], 178,asd)
                                else:
                                    cor.put_char(i, end[1], 178,asd)
                    else:
                        if (node1.flag):
                            node.flag = True
                            node.xs = node1.xs
                            node.ys = node1.ys
                            node.widthX = node1.widthX
                            node.heightY = node1.heightY
                        elif (node2.flag):
                            node.flag = True
                            node.xs = node2.xs
                            node.ys = node2.ys
                            node.widthX = node2.widthX
                            node.heightY = node2.heightY
                        else:
                            node.flag = True

            if tick > 6:
                if (not node.children):
                    if (node.flag):
                        tcod.console_set_default_foreground(con, tcod.white)
                        con.print_frame(node.xs, node.ys, node.widthX, node.heightY, False, tcod.BKGND_NONE)
                        for i in range(node.xs+1, node.xs+node.widthX-1):
                            for j in range(node.ys+1, node.ys+node.heightY-1):
                                con.put_char(i,j,ord('.'),tcod.BKGND_ADD)
                    else:
                        tcod.console_set_default_foreground(con, tcod.white)
                        #if (node.level < tick and node.level > tick - 3):
                        xs = random.randrange(node.x, node.x+node.width//2)
                        ys = random.randrange(node.y, node.y+node.height//2)
                        width = random.randrange((node.x+node.width-xs)//2, (node.x+node.width-xs))
                        height = random.randrange((node.y+node.height-ys)//2, (node.y+node.height-ys))
                        node.flag = True
                        node.xs = xs
                        node.ys = ys
                        node.heightY = height
                        node.widthX = width
                        con.print_frame(xs, ys, width, height, False, tcod.BKGND_NONE)
            else:
                node.flag = False
                if dab:
                    if (node.level < tick and node.level > tick - 3):
                        con.print_frame(node.x, node.y, node.width, node.height, False, tcod.BKGND_NONE)
                else:
                    if (node.level < tick and node.level > tick - 2):
                        con.print_frame(node.x, node.y, node.width, node.height, False, tcod.BKGND_NONE)



        tcod.console_blit(cor, 0, 0, screen_width, screen_height, 0, 0, 0, 1, 1)
        tcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0, 1, 0)
        tcod.console_flush()



if __name__ == '__main__':
     main()
