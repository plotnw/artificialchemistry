class Cell:

    def __init__(self):
        self.opaque = False
        self.solid = True
        self.seen = False
        self.char = ord(' ')

    def makeSeen(self):
        self.seen = True

class Wall(Cell):
    def __init__(self, char):
        self.opaque = True
        self.solid = True
        self.seen = False
        self.char = char

class Floor(Cell):
    def __init__(self):
        self.opaque = False
        self.solid = False
        self.seen = False
        self.char = ord('.')

class Door(Cell):
    def __init__(self):
        self.opaque = False
        self.solid = False
        self.seen = False
        self.char = 206

class Path(Cell):
    def __init__(self):
        self.opaque = False
        self.solid = False
        self.seen = False
        self.char = 176

class Null(Cell):
    def __init__(self):
        self.opaque = True
        self.solid = True
        self.seen = False
        self.char = ord(' ')

class Item(Cell):
    def __init__(self, item):
        self.opaque = False
        self.solid = False
        self.seen = False
        self.item = item
        self.char = item.char

class Stair(Cell):
    def __init__(self):
        self.opaque = False
        self.solid = False
        self.seen = False
        self.char = 240
