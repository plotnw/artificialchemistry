import tcod as tcod
import tcod.event

import random
import math
import subprocess
import string

import rogue.input_handlers as ih
import rogue.entity.entity as entity
import rogue.map.generate as gen
import rogue.message.message as message
import rogue.config as config
import rogue.item.item as item
import rogue.cell as cell
import rogue.map.layout as layoutgen


def main():

    tcod.console_set_custom_font('fonts/tileset.png', tcod.FONT_LAYOUT_ASCII_INROW)

    c_root = tcod.console_init_root(config.SCREEN_WIDTH, config.SCREEN_HEIGHT, 'float', False, tcod.RENDERER_SDL2)
    c_player = tcod.console.Console(config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
    c_enemy = tcod.console.Console(config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
    c_visible = tcod.console.Console(config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
    c_seen = tcod.console.Console(config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
    c_message = tcod.console.Console(config.MESSAGE_WIDTH, config.MESSAGE_HEIGHT)
    c_inventory = tcod.console.Console(config.INVENTORY_WIDTH, config.INVENTORY_HEIGHT)
    c_hud = tcod.console.Console(config.MAP_WIDTH, 7)

    keyAC = tcod.Key()
    mouse = tcod.Mouse()

    # Setting up list of allowed objects
    seed = random.random()
    layout = layoutgen.Layout(seed, config.MAP_HEIGHT, config.MAP_WIDTH)

    while True:
        #tcod.console_set_default_foreground(con, tcod.white)
        c_root.draw_rect(config.MAP_WIDTH, 0, 1, config.SCREEN_HEIGHT, ord('│'), fg=(100,100,255))
        c_root.draw_rect(0, config.MAP_HEIGHT, config.MAP_WIDTH, 1, ord('─'), fg=(100,100,255))
        c_root.draw_rect(config.MAP_WIDTH+1, 27, config.SCREEN_WIDTH, 1, ord('─'),fg=(100,100,255))
        c_visible.print_box(0,0,config.SCREEN_WIDTH,config.SCREEN_HEIGHT,layout.visible(layout.player), fg=(150,175,255))
        layout.messagebox.draw(c_message, config.MESSAGE_WIDTH, config.MESSAGE_HEIGHT)
        layout.player.drawInventory(c_inventory, config.INVENTORY_WIDTH, config.INVENTORY_HEIGHT)
        layout.player.drawEntity(c_player)
        c_seen.print_box(0,0,config.SCREEN_WIDTH,config.SCREEN_HEIGHT,layout.seen(), fg=(75,75,125))
        c_seen.blit(c_root, 0, 0, 0, 0, config.MAP_WIDTH, config.MAP_HEIGHT, bg_alpha=0)
        c_visible.blit(c_root, 0, 0, 0, 0, config.MAP_WIDTH, config.MAP_HEIGHT,bg_alpha=0)
        c_player.blit(c_root, 0, 0, 0, 0, config.SCREEN_WIDTH, config.SCREEN_HEIGHT, bg_alpha=0)
        c_message.blit(c_root, 1+config.MAP_WIDTH, 0, 0, 0, config.MESSAGE_WIDTH, config.MESSAGE_HEIGHT)
        c_inventory.blit(c_root,1+config.MAP_WIDTH, 1+config.MESSAGE_HEIGHT, 0, 0, config.INVENTORY_WIDTH, config.INVENTORY_HEIGHT)

        c_hud.print_box(0,0,config.MAP_WIDTH,7,layout.hud(), fg=(255,255,255))
        c_hud.blit(c_root, 0, 1+config.MAP_HEIGHT, 0, 0, config.MAP_WIDTH, 7)
        tcod.console_flush()
        layout.player.clearEntity(c_player)
        #c_player.put_char(player.x, player.y, ord(' '), tcod.BKGND_NONE)
        c_message.draw_rect(0, 0, config.MESSAGE_WIDTH, config.MESSAGE_HEIGHT, ord(' '))
        c_inventory.draw_rect(0, 0, config.INVENTORY_WIDTH, config.INVENTORY_HEIGHT, ord(' '))
        c_hud.clear()


        #tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS, keyAC, mouse)
        for event in tcod.event.wait():
            eventtype = event.type
            if (eventtype == "QUIT"):
                raise SystemExit()
            elif (eventtype == "KEYDOWN"):
                action = ih.handle_keys(event.sym, event.mod, layout.gamestate)

                move = action.get('move')
                exit = action.get('exit')
                screenshot = action.get('screenshot')
                fullscreen = action.get('fullscreen')
                debugReveal = action.get('debug_reveal')
                combine = action.get('combine')
                mapreset = action.get('debug_mapreset')
                scroll = action.get('scroll')
                pickup = action.get('pickup')
                return0 = action.get('return0')
                keyAC = action.get('keyAC')
                combinedab = action.get('combinedab')
                down = action.get('down')
                equip = action.get('equip')
                keyEquip = action.get('keyEquip')
                returnEquip = action.get('returnEquip')
                debug_heal = action.get('debug_heal')

                if down:
                    if layout.attemptClimb():
                        c_visible.draw_rect(0, 0, config.MAP_WIDTH, config.MAP_HEIGHT, ord(' '))
                        c_seen.draw_rect(0, 0, config.MAP_WIDTH, config.MAP_HEIGHT, ord(' '))
                        c_seen.blit(c_root, 0, 0, 0, 0, config.MAP_WIDTH, config.MAP_HEIGHT, bg_alpha=1)
                        c_visible.blit(c_root, 0, 0, 0, 0, config.MAP_WIDTH, config.MAP_HEIGHT,bg_alpha=1)
                        tcod.console_flush()
                if debug_heal:
                    layout.player.hp  =layout.player.maxhp
                if equip:
                    layout.gamestate = 2
                if scroll:
                    layout.scrollMsgBox(scroll)
                if pickup:
                    layout.attemptPickup()
                if move:
                    layout.movePlayer(move)
                if screenshot:
                    layout.screenshot()
                if debugReveal:
                    layout.revealAll()
                if mapreset:
                    layout.messagebox.addMessage("Debug: Layout Reset")
                    c_visible.draw_rect(0, 0, config.MAP_WIDTH, config.MAP_HEIGHT, ord(' '))
                    c_seen.draw_rect(0, 0, config.MAP_WIDTH, config.MAP_HEIGHT, ord(' '))
                    c_seen.blit(c_root, 0, 0, 0, 0, config.MAP_WIDTH, config.MAP_HEIGHT, bg_alpha=1)
                    c_visible.blit(c_root, 0, 0, 0, 0, config.MAP_WIDTH, config.MAP_HEIGHT,bg_alpha=1)
                    tcod.console_flush()

                    seed = random.random()
                    layout = gen.generateLayout(seed, config.MAP_HEIGHT, config.MAP_WIDTH)
                if return0:
                    layout.combineStop()
                if combinedab:
                    layout.combineFinish()
                if keyAC:
                    layout.combineSelect(keyAC)
                if combine:
                    layout.combineStart()
                if keyEquip:
                    layout.equip(keyEquip)
                if (returnEquip):
                    layout.gamestate = 0
                if exit:
                    raise SystemExit()

                layout.playerProcess()
                layout.cullMobs()
                layout.moveMobs()
                layout.processDeath()




if __name__ == '__main__':
     main()
