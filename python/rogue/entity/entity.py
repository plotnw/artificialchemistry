import tcod
import numpy as np
import string
import math
import sys
import rogue.item.item as item
import random

class Entity:

    def __init__(self, x, y, char, hp=10, sight=5):
        self.x = x
        self.y = y
        self.char = char
        self.hp = hp
        self.sight = sight
        self.weights = [0,0,0,0,0]

    def move(self, dx, dy):
        # Move the entity by a given amount
        self.x += dx
        self.y += dy

    def drawEntity(self, con):
        con.put_char(self.x, self.y, self.char, tcod.BKGND_NONE)

    def clearEntity(self, con):
        con.put_char(self.x, self.y, ord(' '), tcod.BKGND_NONE)

    def attack(self, weights):
        acc = 0
        for i in range(0, 5):
            acc = acc + int((3 * (weights[i] + 1) / (self.weights[i]+1)))

        self.hp = self.hp - acc
        return acc

    def fov(self, layout):
        transparent = tcod.map.compute_fov(np.array([np.array([not(yi.opaque) for yi in xi]) for xi in layout.layout]), self.x, self.y, self.sight, True, tcod.FOV_SHADOW)
        return transparent

class Player(Entity):

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.char = 3
        self.inventory = []
        self.hp = 1000
        self.maxhp = 1000
        self.sight = 10
        self.cooldown = 10
        self.weapon = None
        self.weights = [0,0,0,0,0]

    def addInventory(self, item):
        self.inventory.append(item)

    def drawInventory(self, con, width, height):
        tempoffset = 0
        offset = 0
        for s in self.inventory:
            temp = math.ceil(len(s.name)/width)
            con.print_box(0, tempoffset, width, temp, string.ascii_lowercase[offset] + ") " + s.name, fg=(200, 200, 255))
            tempoffset = tempoffset + temp
            offset = offset + 1
            if (tempoffset > height):
                break

    def heal(self):
        if (self.cooldown > 0):
            self.cooldown = self.cooldown - 1
        else:
            if (self.hp < self.maxhp):
                self.hp = self.hp + 1
                self.cooldown = 10

class Enemy(Entity):

    def __init__(self, xp, yp, obj):
        w = 0
        y = 0
        x = 0
        z = 0
        v = 0
        for c in obj:
            if (c == "W"):
                w = w + 1
            if (c == "X"):
                x = x + 1
            if (c == "Y"):
                y = y + 1
            if (c == "Z"):
                z = z + 1
            if (c == "V"):
                v = v + 1
        self.weights = [v,w,x,y,z]
        self.x = xp
        self.y = yp
        self.char = ord("VWXYZ"[self.weights.index(max(self.weights))])
        #self.char = ord('w')
        self.hp = 100
        self.sight = 3
        self.target = [xp,yp]
        self.damage = 5
        self.object = obj

    def move_astar(self, target, layout, my_path):
        dx = target.x - self.x
        dy = target.y - self.y
        distance = math.sqrt(dx ** 2 + dy ** 2)
        if (distance < 2):
            #layout.messagebox.addMessage("smack attack")
            ret = target.attack(self.weights)
            layout.messagebox.addMessage("you were hit for " + str(ret))
        else:

            tcod.path_compute(my_path, self.x, self.y, target.x, target.y)

            if not tcod.path_is_empty(my_path) and tcod.path_size(my_path) < 25:
                x, y = tcod.path_walk(my_path, True)
                if x or y:
                    self.x = x
                    self.y = y
            else:
                m = self.move_towards(target.x, target.y, layout)
    def move_towards(self, target_x, target_y, layout):
        dx = target_x - self.x
        dy = target_y - self.y
        distance = math.sqrt(dx ** 2 + dy ** 2)
        if (distance < 2):
            layout.messagebox.addMessage("smack attack")

        dx = int(round(dx / distance))
        dy = int(round(dy / distance))

        if not (layout.solid(self.y + dy, self.x + dx) or layout.mobAt(self.x + dx, self.y + dy) or (layout.player.x == self.x+dx and layout.player.y == self.y+dy)):
            self.move(dx, dy)








