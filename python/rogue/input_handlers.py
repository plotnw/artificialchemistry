import tcod as tcod


def handle_keys(key, mod, gamestate):
    #print(key)
    #print(key)
    # Movement keys
    #key = chr(key)
    if (gamestate == 0):
        if mod & tcod.event.KMOD_SHIFT and key == ord('.'):
            return {'down': True}
        if key == ord('g'):
            return {'pickup': True}
        if key == ord('w'):
            return {'debug_heal': True}
        if key == ord('y'):
            return {'move': (-1, -1)}
        if key == ord('k'):
            return {'move': (0, -1)}
        if key == ord('u'):
            return {'move': (1, -1)}
        if key == ord('h'):
            return {'move': (-1, 0)}
        if key == ord('l'):
            return {'move': (1, 0)}
        if key == ord('b'):
            return {'move': (-1, 1)}
        if key == ord('j'):
            return {'move': (0, 1)}
        if key == ord('n'):
            return {'move': (1, 1)}
        if key == ord('.'):
            return {'move': (0,0)}
        if (key == ord('p')):
            return {'screenshot': True}
        if (key == ord('[')):
            return {'debug_reveal': True}
        if (key == ord(']')):
            return {'combine': True}
        if (key == ord('\\')):
            return {'debug_mapreset': True}
        if key == 1073741906:
            return {'scroll': 2}
        if key == 1073741905:
            return {'scroll': 1}
        if key == ord('e'):
            return {'equip': True}
    elif (gamestate == 1):
        if (key == 27):
            return {'return0' : True}
        if (key == 13):
            return {'combinedab' : True}
        elif (key >= 0 and key <256):
                return {'keyAC': chr(key)}
    elif (gamestate == 2):
        if (key == 27):
            return {'returnEquip' : True}
        if (key >= 0 and key < 256):
            return {'keyEquip': chr(key)}
    #if key == 27:
        # Exit the game
        #return {'exit': True}

    # No key was pressed
    return {}
