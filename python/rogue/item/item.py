class Item:

    def __init__(self, name="Default Item", lore="Default Lore"):
        self.char = ord('%')
        self.name = name
        self.lore = lore

class Chemical:

    def __init__(self, object):
        self.char = ord('\"')
        self.name = object
        self.lore = "A strange blue chemical."
        self.object = object

class Weapon:

    def __init__(self, i):
        self.char = ord('/')
        self.name = weaponList[i][1]
        self.lore = weaponList[i][2]
        self.damage = weaponList[i][0]

weaponList = [
        (7, "sword", ""),
        (10, "SWORD", ""),
        (5, "hands", "")
        ]

itemlist = [
        ("Iron Mail", "To protect your noggin"),
        ("Fancy Hat", "Not very effective, but very swanky"),
        ("Rusty Nail", "Very, very slow acting posion damage"),
        ("Wand of Summoning \"Wand of Summoning Wand\" Wand", "One use only")
        ]

def giveMeAnItem(seed):
    return Weapon(seed)

