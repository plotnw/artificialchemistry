import tcod.bsp

import numpy as np
import random

import rogue.cell as cell
import rogue.map.layout as layout

def mapSet(map, x, y, v):
    m = map[x,y]
    a = [2,3,4,5,6,7]
    if (m in a and v == 8):
        map[x,y] = 9
    elif (m == 0):
        map[x,y] = v


def generateIntGrid(seed, heightI, widthI):
    bsp = tcod.bsp.BSP(x=0, y=0, width=widthI, height=heightI)
    bsp.split_recursive(
        depth=5,
        min_width=10,
        min_height=10,
        max_horizontal_ratio=1,
        max_vertical_ratio=1,
        seed=tcod.random.Random(tcod.random.MERSENNE_TWISTER, seed).random_c,
    )

    random.seed(seed)
    map = np.zeros((widthI, heightI))

    # In pre order, leaf nodes are visited before the nodes that connect them.
    # We do post order, since we need to add some extra data to the nodes first
    for node in bsp.post_order():
        if node.children:
            node1, node2 = node.children
            if (node1.flag and node2.flag):
                #map[node1.xs + node1.widthX//2,node1.ys + node1.heightY//2] = 8
                #map[node2.xs + node2.widthX//2,node2.ys + node2.heightY//2] = 8
                node.flag = True
                node.xs = node2.xs
                node.ys = node2.ys
                node.widthX = node2.widthX
                node.heightY = node2.heightY

                start = [node1.xs+int(random.uniform(.25, .75)*node1.widthX), node1.ys+int(random.uniform(.25,.75)*node1.heightY)]
                end = [node2.xs+int(random.uniform(.25,.75)*node2.widthX), node2.ys+int(random.uniform(.25,.75)*node2.heightY)]
                if (abs(end[1]-start[1]) > abs(end[0]-start[0])):
                    cutoff = int(random.uniform(end[1], start[1]))
                    if (cutoff in [node1.ys, node1.ys+node1.heightY, node2.ys, node2.ys+node2.heightY]):
                        cutoff = cutoff - 1
                    for i in range(end[1], start[1], 1 if (end[1]<start[1]) else -1):
                        if (i < cutoff):
                            #map[start[0],i]=8
                            mapSet(map, start[0], i, 8)
                        elif (i == cutoff):
                            for j in range(end[0], start[0], 1 if (end[0]<start[0]) else -1):
                                #map[j,i] = 8
                                mapSet(map, j, i, 8)
                            mapSet(map, start[0], i, 8)
                        else:
                            #map[end[0], i]=8
                            mapSet(map, end[0], i, 8)
                else:
                    cutoff = int(random.uniform(end[0], start[0]))
                    if (cutoff in [node1.xs, node1.xs+node1.widthX, node2.xs, node2.xs+node2.widthX]):
                        cutoff = cutoff - 1
                    for i in range(end[0], start[0], 1 if (end[0]<start[0]) else -1):
                        if (i < cutoff):
                            mapSet(map, i, start[1], 8)
                        elif (i == cutoff):
                            for j in range(end[1], start[1], 1 if (end[1]<start[1]) else -1):
                                mapSet(map, i, j, 8)
                            mapSet(map, i, start[1], 8)
                        else:
                            mapSet(map, i, end[1], 8)
            else:
                if (node1.flag):
                    node.flag = True
                    node.xs = node1.xs
                    node.ys = node1.ys
                    node.widthX = node1.widthX
                    node.heightY = node1.heightY
                elif (node2.flag):
                    node.flag = True
                    node.xs = node2.xs
                    node.ys = node2.ys
                    node.widthX = node2.widthX
                    node.heightY = node2.heightY
                else:
                    node.flag = False
        else:
            #print('Dig a room for %s.' % node)
            xs = random.randrange(node.x, node.x+node.width//2)
            ys = random.randrange(node.y, node.y+node.height//2)
            width = random.randrange((node.x+node.width-xs)//2, (node.x+node.width-xs))
            height = random.randrange((node.y+node.height-ys)//2, (node.y+node.height-ys))
            if (width < 3):
                width = 3
            if (height < 3):
                height = 3
            for i in range (xs, xs+width):
                map[i,ys] = 6
                map[i,ys+height-1] = 6
            for i in range (ys, ys+height):
                map[xs,i] = 7
                map[xs+width-1,i] = 7
            map[xs,ys]=2
            map[xs,ys+height-1]=3
            map[xs+width-1,ys]=4
            map[xs+width-1,ys+height-1]=5
            for i in range(xs+1, xs+width-1):
                for j in range(ys+1, ys+height-1):
                    map[i,j]=1
            # Look
            # If python didn't want me to do this
            # It shouldn't have let me
            # Adding runtime instance variables is fine, right?
            # See I even add a flag saying whether I did or not
            node.flag = True
            node.xs = xs
            node.ys = ys
            node.heightY = height
            node.widthX = width
    # we need to add something to track the number of rooms for placing singleton things
    # like stairs, for example
    # but we repeated room positions earlier to ensure the entire dungeon is connected, duplicated into nodes with children specifically
    # so just remove the flag again
    for node in bsp.post_order():
        if node.children:
            node.flag = False

    #for node in bsp.post_order():
        #if (node.flag):
            #map[node.xs + node.widthX//2,node.ys + node.heightY//2] = 8
            #we can add items h, other things to room
            #each node is unique room

    for x in range(1, widthI-1):
        for y in range(1, heightI-1):
            if (map[x,y] == 9):
                if not (8 in [map[x-1,y], map[x+1,y],map[x,y-1],map[x,y+1]]):
                    if (0 == map[x-1,y] or 0 == map[x+1,y]):
                        map[x,y] = 7
                    else:
                        map[x,y] = 6
    return (map, bsp)
    '''dab = ""
    for y in range(0, 60):
        for x in range(0, 80):
            m = map[x,y]
            if (m == 1):
                dab = dab + '.'
            elif (m == 2):
                dab = dab + chr(201) # corner
            elif (m == 3):
                dab = dab + chr(200) # corner
            elif (m == 4):
                dab = dab + chr(187) # corner
            elif (m == 5):
                dab = dab + chr(188) # corner
            elif (m == 6):
                dab = dab + chr(205) # upwards
            elif (m == 7):
                dab = dab + chr(186) # sideways
            elif (m == 8):
                dab = dab + chr(176) # light dither block
            elif (m == 9):
                dab = dab + chr(206) # cross wall
            else:
                dab = dab + ' '
        dab = dab + '\n'
    return dab'''

def intToCell(m):
    if (m == 1):
        return cell.Floor()
    elif (m == 2):
        return cell.Wall(201)
    elif (m == 3):
        return cell.Wall(200)
    elif (m == 4):
        return cell.Wall(187)
    elif (m == 5):
        return cell.Wall(188)
    elif (m == 6):
        return cell.Wall(205)
    elif (m == 7):
        return cell.Wall(186)
    elif (m == 8):
        return cell.Path()
    elif (m == 9):
        return cell.Door()
    else:
        return cell.Null()

def intGridToLayout(map, height, width):
    layout = []
    for i in range (0, height):
        line = []
        for j in range(0, width):
            line.append(intToCell(map[j, i]))
        layout.append(line)
    return layout

def generateLayout(seed, height, width):
    a,c = generateIntGrid(seed, height, width)
    b = np.array(intGridToLayout(a, height, width))
    return (b, c, seed, height, width)

def findSpace(bsp):
    m = 0
    for i in bsp.post_order():
        m = m + 1
    d = random.randrange(0, m)
    a = 0
    for n in bsp.post_order():
        if (a == d):
            x = random.randrange(n.xs+1, n.xs+n.widthX-1)
            y = random.randrange(n.ys+1, n.ys+n.heightY-1)
            return (x,y)
        else:
            a = a + 1
    print("oops")
