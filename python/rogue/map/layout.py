import tcod.map
import numpy as np
import random
import subprocess
import string

import rogue.item.item as item
import rogue.cell as cell
import rogue.entity.entity as entity
import rogue.message.message as message
import rogue.map.generate as gen


class Layout:

    # TODO: MAKE LESS COPY PAST FOR THESE TWO METHODS
    def __init__(self, seed, height, width):
        a = gen.generateLayout(seed, height, width)
        self.layout = a[0]
        self.playerturn = True
        self.bsp = a[1]
        self.height = height
        self.width = width
        self.messagebox = message.Message()
        self.objects = subprocess.run(["stack exec artificialChemistry-rogue init"],shell=True,capture_output=True,text=True).stdout.split('\n')
        self.objects.pop(-1)
        self.level = 0
        self.mobs = self.monsterSpawns()
        self.addItems(self.objects)
        m = gen.findSpace(self.bsp)
        self.player = entity.Player(m[0], m[1])
        n = gen.findSpace(self.bsp)
        self.layout[n[1],n[0]] = cell.Stair()
        self.gamestate = 0
        self.selected = []
        self.seed = seed
        self.messagebox.addMessage("Welcome to Level " + str(self.level))

    def incrementLevel(self):
        self.level = self.level + 1
        alayout = gen.generateLayout(self.seed+self.level, self.height, self.width)
        self.layout = alayout[0]
        self.bsp = alayout[1]
        self.mobs = self.monsterSpawns()
        self.addItems(self.objects)
        m = gen.findSpace(self.bsp)
        self.player.x = m[0]
        self.player.y = m[1]
        n = gen.findSpace(self.bsp)
        self.layout[n[1],n[0]] = cell.Stair()
        self.messagebox.addMessage("Welcome to Level " + str(self.level))

    def attemptClimb(self):
        if (issubclass(type(self.layout[self.player.y,self.player.x]), cell.Stair)):
            self.incrementLevel()
            return True
        else:
            self.messagebox.addMessage("You can't go down a floor - you need stairs")
            return False



    def __str__(self):
        dab = ""
        for y in range(0, self.height):
            for x in range(0, self.width):
                m = self.layout[y,x]
                dab = dab + chr(m.char)
            dab = dab + '\n'
        return dab

    def processDeath(self):
        if (self.player.hp <= 0 and self.gamestate != -1):
            self.messagebox.addMessage("You died - this is so sad")
            self.gamestate = -1

    def hud(self):
        return str(self.player.hp) + " -------- " + ("None" if self.player.weapon == None else self.player.weapon.name) + " -------- " + str(self.player.weights)

    def pickupItem(self, x, y):
        if (issubclass(type(self.layout[y,x]), cell.Item)):
            a = self.layout[y,x].item
            self.layout[y,x] = cell.Floor()
            return a

    def attemptPickup(self):
        b = self.pickupItem(self.player.x, self.player.y)
        if (b == None):
            self.messagebox.addMessage("No Item")
        else:
            self.player.inventory.append(b)
            self.messagebox.addMessage("Picked up: " + b.name)
            self.playerturn = False

    def movePlayer(self, move):
        dx, dy = move
        if not (self.solid(self.player.y+dy, self.player.x+dx)):
            mob = self.mobAt(self.player.x+dx, self.player.y+dy)
            if (mob != None):
                ret = mob.attack(self.player.weights)
                self.messagebox.addMessage("Hit for " + str(ret) + " damage")
            else:
                self.player.move(dx, dy)
                if (issubclass(type(self.layout[self.player.y,self.player.x]), cell.Item)):
                    self.messagebox.addMessage(self.layout[self.player.y,self.player.x].item.lore)
                    self.messagebox.addMessage(self.layout[self.player.y,self.player.x].item.name)
            self.playerturn = False
        else:
            self.messagebox.addMessage("You cannot move through a wall. That is solid.")
            self.playerturn = True

    def addItems(self, objects):
        merp = objects.copy()
        merp = list(filter(lambda x: len(x) < 5, merp))
        for node1 in self.bsp.post_order():
            if (node1.flag):
                area = node1.widthX * node1.heightY
                for i in range(1,area // 20):
                    position = [random.randint(node1.xs+1, node1.xs+node1.widthX-2), random.randint(node1.ys+1, node1.ys+node1.heightY-2)]
                    if (random.random() > 1):
                        #a = item.itemlist[random.randint(0, len(item.itemlist)-1)]
                        self.layout[position[1],position[0]] = cell.Item(item.giveMeAnItem(random.randint(0, 3) % 2))

                    else:
                        self.layout[position[1],position[0]] = cell.Item(item.Chemical(merp[random.randint(0,len(merp)-1)]))

    def monsterSpawns(self):
        moblist = []
        for node1 in self.bsp.post_order():
            if (node1.flag):
                position = [random.randint(node1.xs+1, node1.xs+node1.widthX-2), random.randint(node1.ys+1, node1.ys+node1.heightY-2)]
                moblist.append(entity.Enemy(position[0], position[1], self.getObject(self.level)))
        return moblist

    def getObject(self, i):
        m = []
        for j in range (0, 10):
            for e in self.objects:
                if (len(e) < i + j):
                    m.append(e)
            if (len(m) > 0):
                break
        return m[random.randint(0, len(m)-1)]

    def mobAt(self, x, y):
        for mob in self.mobs:
            if (mob.x == x and mob.y == y):
                return mob
        return None

    def cullMobs(self):
        for mob in self.mobs:
            if (mob.hp <= 0):
                self.mobs.remove(mob)
                self.layout[mob.y, mob.x] = cell.Item(item.Chemical(mob.object))
                self.messagebox.addMessage("Killed a monster")

    def moveMobs(self):
        if (not self.playerturn):
            for mob in self.mobs:
                if ((mob.x-self.player.x)**2+(mob.y-self.player.y)**2 < 100):
                    self.moveMob(mob)
            self.playerturn = True

    def moveMob(self, mob):
        fovA = self.solidMap()
        asdmap = tcod.map.Map(width=self.width,height=self.height)
        my_path = tcod.path_new_using_map(asdmap, 1.41)
        for y1 in range(self.height):
            for x1 in range(self.width):
                asdmap.walkable[y1,x1]=fovA[y1,x1]
        mob.move_astar(self.player, self, my_path)
        #transparent = mob.fov(self.layout)
        #if (transparent[self.player.y, self.player.x]):
            #self.messagebox.addMessage("seen")

    def solid(self, xP, yP):
        if(self.layout[xP,yP].solid):
            return True
        if (xP==self.player.x and yP==self.player.y):
            return True
        return False

    def solidMap(self):
        a = np.array([np.array([not (yi.solid) for yi in xi]) for xi in self.layout])
        #a[self.player.y,self.player.x] = False
        for m in self.mobs:
            a[m.y,m.x] = False
        return a

    def visible(self, entity):
        transparent = entity.fov(self)
        dab = ""
        for y in range(0, self.height):
            for x in range(0, self.width):
                if (transparent[y,x]):
                    m = self.layout[y,x]
                    m.seen = True
                    mob_q = self.mobAt(x,y)
                    if (mob_q == None):
                        dab = dab + chr(m.char)
                    else:
                        dab = dab + chr(mob_q.char)
                else:
                    dab = dab + ' '
            dab = dab + '\n'
        return dab

    def screenshot(self):
        self.messagebox.addMessage("Took a screenshot")
        tcod.sys_save_screenshot()

    def scrollMsgBox(self, scroll):
        if scroll == 1:
            self.messagebox.scrollDown()
        elif scroll == 2:
            self.messagebox.scrollUp()

    def revealAll(self):
        self.messagebox.addMessage("Debug: Reveal All")
        for y in range (0, self.height):
            for x in range(0, self.width):
                self.layout[y,x].seen = True

    def seen(self):
        dab = ""
        for y in range(0, self.height):
            for x in range(0, self.width):
                m = self.layout[y,x]
                if (m.seen):
                    dab = dab + chr(m.char)
                else:
                    dab = dab + ' '
            dab = dab + '\n'
        return dab

    def combineStart(self):
        self.gamestate = 1
        self.messagebox.addMessage("Begin combining")

    def combineStop(self):
        self.gamestate = 0
        for g in self.selected:
            self.player.inventory.append(g)
        self.selected = []
        self.messagebox.addMessage("Stop Combining")

    def combineFinish(self):
        self.messagebox.addMessage("====")
        stringc = "stack exec artificialChemistry-rogue "
        for m in self.selected:
            stringc = stringc + m.name + " "
        t = subprocess.run([stringc], shell=True, capture_output = True, text=True).stdout.split("\n")
        t = t[:-1]
        for g in t:
            self.player.inventory.append(item.Chemical(g))
            self.messagebox.addMessage("You got: " + g)
        self.gamestate = 0
        self.playerturn = False
        self.selected = []

    def combineSelect(self, key):
        try:
            obj = self.player.inventory[ord(key)-97]
            self.selected.append(obj)
            self.player.inventory.remove(obj)
            self.messagebox.addMessage("Combine: " + obj.name)
        except:
            self.messagebox.addMessage("no item in slot")

    def playerProcess(self):
        if (self.playerturn == False):
            self.player.heal()

    def equip(self, key):
        #print(ord(key)-97)
        #try:
        try:
            obj = self.player.inventory[ord(key) - 97]
        except:
            return 0
        self.player.inventory.remove(obj)
        if not (self.player.weapon == None):
            self.player.inventory.append(self.player.weapon)
        self.player.weapon = obj
        self.messagebox.addMessage("Equipped " + self.player.weapon.name)
        w = 0
        y = 0
        x = 0
        z = 0
        v = 0
        for c in obj.name:
            if (c == "W"):
                w = w + 1
            if (c == "X"):
                x = x + 1
            if (c == "Y"):
                y = y + 1
            if (c == "Z"):
                z = z + 1
            if (c == "V"):
                v = v + 1
        self.player.weights = [v,w,x,y,z]
        #if (issubclass(type(obj), item.Weapon)):
            #self.player.inventory.remove(obj)
            #if not (self.player.weapon == None):
                #self.player.inventory.append(self.player.weapon)
            #self.player.weapon = obj
            #self.messagebox.addMessage("Equipped " + obj.name)
        #else:
            #self.messagebox.addMessage("You have to wield a weapon!")
        #except:
            #self.messagebox.addMessage("no item in slot")
        self.gamestate = 0












