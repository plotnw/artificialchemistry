import math
class Message:

    def __init__(self):
        self.messages = ["Welcome to Rogue"]
        self.offset = 0

    def addMessage(self, message):
        self.messages.append(message)
        self.offset = 0

    def scrollDown(self):
        self.offset = self.offset + 1

    def scrollUp(self):
        self.offset = self.offset - 1
        if (self.offset < 0):
            self.offset = 0

    def draw(self, con, width, height):
        tempoffset = 0
        self.messages.reverse()
        for s in self.messages[self.offset:]:
            temp = math.ceil(len(s)/width)
            con.print_box(0, tempoffset, width, temp, s, fg=(200, 200, 255))
            tempoffset = tempoffset + temp
            if (tempoffset > height):
                break
        self.messages.reverse()
