module Main where

import System.Environment
import Data.List
import ArtificialChemistry.ACGen
import System.Random
import ArtificialChemistry.PatternMatching
import ArtificialChemistry.Data hiding (main)
import Util

main :: IO()
main = do
    args <- getArgs
    -- never pass not one args
    if (args !! 0) == "init"
       then initialize
       else process $ args

initialize :: IO()
initialize = do
    g <- newStdGen
    b <- randomIO
    --print $ b
    let m = testACGenExact b
    mapM_ (putStrLn . showAC) (getUniqueObjs m)
    writeFile "acgen.dat" $ show $ map showAC $ acgen_rules m

process :: [String] -> IO()
process input = do
    m <- readFile "acgen.dat"
    let rs = map (\x -> (readAC x) :: RecombinationRule Token) $ ((read m) :: [String])
    let os = map (\x -> (readAC x) :: Object Token) input
    let possibilities = map (\x -> getPossibleRecombinationRule os x) rs
    let po = filter (\(b, x) -> b /= []) possibilities
    i <- randomIO
    j <- randomIO
    if (length po == 0)
        then do mapM_ putStrLn input
        else do
            let intermeme = po !! (i `mod` (length po))
            let objm = (fst intermeme) !! (j `mod` (length (fst intermeme)))
            let rule = snd intermeme
            let result = applyRecombinationRule os objm rule
            mapM_ (putStrLn . showAC) result
