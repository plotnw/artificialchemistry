module ArtificialChemistry.ACGen where


import ArtificialChemistry.Data hiding (rules)
import ArtificialChemistry.ACUtil
import ArtificialChemistry.Random
import ArtificialChemistry.RandomRR
import Util
import Data.List
import Data.Matrix

import System.Random

seedElementListSize :: Int
seedElementListSize = 100

maxTakeObject :: Int
maxTakeObject = 3

makeObjectList :: (Eq a) => [a] -> StdGen -> [Object a]
makeObjectList a stdgen = take seedElementListSize $ map (\x -> generateRandomObject (intToListElement a) x) (map mkStdGen (randoms stdgen))

data ACGen a = ACGen {acgen_tokenDomain :: [a], acgen_objects :: [Object a], acgen_rules :: [RecombinationRule a], acgen_equivs :: [(a, [a])], acgen_stdgen :: StdGen}

equivList :: [(Token, [Token])]
equivList = [(V, [A, B, C]),
             (W, [D, E, F, G]),
             (X, [H, I, J]),
             (Y, [K, L, M, N, O, P]),
             (Z, [Q, R, S, T, U])
            ]

instance (Show a, ShowAC a) => Show (ACGen a) where
    show (ACGen domain objs rules equivs stdgen) = "ACGen {\nStdGen {" ++ show stdgen ++ "}\nDomain {" ++ concat (map (\x -> showAC x ++ ", ") domain) ++ "}\nObjs {" ++ concat (map (\x -> showAC x ++ ", ") objs) ++ "}\nRules {\n" ++ concat (map (\x -> showAC x ++ ",\n") rules) ++ "}\nEquivs" ++ concat (map (\x -> show x ++ ", ") equivs) ++ "}" 

instance (Eq a) => Eq (ACGen a) where
    a == b = (acgen_objects a == acgen_objects b) && (acgen_rules a == acgen_rules b)

createACGen :: (Eq a) => [a] -> [(a,[a])] -> StdGen -> ACGen a
createACGen list equivs stdgen = (ACGen list (makeObjectList list (fst m)) [] equivs (snd m))
    where m = split stdgen

createACGenSmall :: (Eq a) => [a] -> [(a,[a])] -> StdGen -> ACGen a
createACGenSmall list equivs stdgen = (ACGen list (map makeSingletonObject list) [] equivs (snd m))
    where m = split stdgen

makeSingletonObject :: a -> Object a
makeSingletonObject m = [(0, [m])]

addRuleACGen :: (Eq a) => ACGen a -> ACGen a
addRuleACGen (ACGen domain objs rules equivs stdgen) = ACGen domain objs (newrule:rules) equivs new
    where bad = randoms stdgen
          new = mkStdGen (bad !! 0)
          rnum = 1 + ((bad !! 1) `mod` (maxObjectRR-minObjectRR))
          pnum = 1 + ((bad !! 2) `mod` (maxObjectRR-minObjectRR))
          rc = take rnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 3)))
          pc = take pnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 4)))
          newrule = makeRecombinationRuleConstrained (rc, pc) (map PatternElement domain) (mkStdGen (bad !! 5))

addRuleExactACGen :: (Eq a) => Int -> Int -> ACGen a -> ACGen a
addRuleExactACGen rnum pnum (ACGen domain objs rules equivs stdgen) = ACGen domain (objs++map objectPatternToObject (snd5 newrule)) (newrule:rules) equivs new
    where bad = randoms stdgen
          new = mkStdGen (bad !! 0)
          rc = take rnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 3)))
          pc = take pnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 4)))
          newrule = makeRecombinationRuleExact (rc, pc) (map PatternElement domain) (mkStdGen (bad !! 5))

addRuleExactSingleton :: (Eq a) => Int -> Int -> ACGen a -> ACGen a
addRuleExactSingleton rnum pnum (ACGen domain objs rules equivs stdgen) = ACGen domain (objs) (newrule:rules) equivs new
    where bad = randoms stdgen
          new = mkStdGen (bad !! 0)
          rc = take rnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 3)))
          pc = take pnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 4)))
          newrule = makeRecombinationRuleSingletons (rc, pc) (map PatternElement domain) (mkStdGen (bad !! 5))


addRuleExactBeginACGen :: (Eq a) => ACGen a -> ACGen a
addRuleExactBeginACGen (ACGen domain objs rules equivs stdgen) = ACGen domain (objs++objs++map objectPatternToObject (snd5 newrule)) (newrule:rules) equivs new
    where bad = randoms stdgen
          new = mkStdGen (bad !! 0)
          rnum = 2
          pnum = 0
          rc = take rnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 3)))
          pc = take pnum $ map objectToObjectPattern $ map (\x -> objs !! (x `mod` (length objs))) (randoms (mkStdGen (bad !! 4)))
          newrule = makeRecombinationRuleExact (rc, pc) (map PatternElement domain) (mkStdGen (bad !! 5))

addRuleReverse :: (Eq a) => ACGen a-> ACGen a
addRuleReverse (ACGen domain objs rules equivs stdgen) = ACGen domain objs (newrule:rules) equivs new
    where bad = randoms stdgen
          new = mkStdGen (bad !! 0)
          newrule = (\(a,b,c,d,e)->(b,a,c,d,e)) (rules !! ((bad !! 1) `mod` (length rules)))

addRulesMutation :: (ShowAC a, ReadAC a, Eq a) => Int -> ACGen a -> ACGen a
addRulesMutation i a@(ACGen domain objs rules equivs stdgen) = ACGen domain newobjs (newrules++rules) equivs new
    where bad = randoms stdgen
          new = mkStdGen (bad !! 0)
          newrules = take i $ (map (\x -> addRuleMutation a (mkStdGen x)) (drop 1 bad))
          partialACGen = ACGen domain objs (newrules++rules) equivs new
          newobjs = getUniqueObjs partialACGen


addRuleMutation :: (ShowAC a, ReadAC a, Eq a) => ACGen a -> StdGen -> RecombinationRule a
addRuleMutation a@(ACGen domain objs rules equivs _) stdgen = addRuleMutation' new dab rule
    where bad = randoms stdgen
          new = mkStdGen (bad !! 0)
          --hardcoded amount of five, since im a bad programmer
          --currently this ensure that every time, we end up replacing all tokens
          --we dont take those - turn this into a list, and pass that to addRuleMutation
          equiv1 = if (bad !! 7) `mod` 3 /= 1 then [(fst (equivs !! 0), (snd (equivs !! 0)) !! ((bad !! 1) `mod` (length (snd (equivs !! 0)))))] else []
          equiv2 = if (bad !! 7) `mod` 3 /= 1 then [(fst (equivs !! 1), (snd (equivs !! 1)) !! ((bad !! 2) `mod` (length (snd (equivs !! 1)))))] else []
          equiv3 = if (bad !! 7) `mod` 3 /= 1 then [(fst (equivs !! 2), (snd (equivs !! 2)) !! ((bad !! 3) `mod` (length (snd (equivs !! 2)))))] else []
          equiv4 = if (bad !! 7) `mod` 3 /= 1 then [(fst (equivs !! 3), (snd (equivs !! 3)) !! ((bad !! 4) `mod` (length (snd (equivs !! 3)))))] else []
          equiv5 = if (bad !! 7) `mod` 3 /= 1 then [(fst (equivs !! 4), (snd (equivs !! 4)) !! ((bad !! 5) `mod` (length (snd (equivs !! 4)))))] else []
          rule = rules !! ((bad !! 6) `mod` (length rules))
          dab = equiv1 ++ equiv2 ++ equiv3 ++ equiv4 ++ equiv5
          
        --ACGen domain objs++(map objectPatternToObject (snd5 newrule)) (newrule:rules) equivs new
          --newACGen = if (containsRR rule (fst equiv)) then addRuleMutation' a new equiv1 equiv2 equiv3 equiv4 equiv5 rule else (ACGen domain objs rules equivs new)

addRuleMutation' :: (ShowAC a, ReadAC a, Eq a) => StdGen -> [(a, a)] -> RecombinationRule a -> RecombinationRule a
addRuleMutation' stdgen dab rule = newrule
    where bad = randoms stdgen
          new = mkStdGen (bad !! 1)
          newrule = foldl' (\x (a,b) -> replaceTokenRecombinationRule a b x) rule dab

--mutateReplace

--mutateRemove

--mutateAdd 


simplifyACGen :: (Eq a) => ACGen a -> ACGen a
simplifyACGen a@(ACGen domain objs rules equivs stdgen) = ACGen domain (getUniqueObjs a) (nub rules) equivs stdgen

getUniqueObjs :: (Eq a) => ACGen a -> [Object a]
getUniqueObjs (ACGen domain objs rules equivs stdgen) = nub $ concat $ map (\(r,p,a,b,c) -> (map objectPatternToObject r) ++ (map objectPatternToObject p)) rules

addRules :: (Eq a) => (ACGen a -> ACGen a) -> Int -> ACGen a -> ACGen a
addRules f i a = (iterate f a) !! i

testACGenExact :: Int -> ACGen Token
--testACGenExact a = addRules (addRuleMutation) 5 $ addRules (addRuleExactACGen 2 0) 50 $ addRules addRuleExactBeginACGen 2 (createACGenSmall tokenListRestricted equivList (mkStdGen a))
testACGenExact a = addRules (addRuleReverse) 200 $ addRules (addRuleExactSingleton 2 1) 40 $ addRules (addRuleExactACGen 2 1) 300 $ addRules (addRuleExactACGen 2 0) 300 $ addRulesMutation 150 $ expandDomain $ addRules (addRuleExactACGen 2 0) 75 $ addRules addRuleExactBeginACGen 2 (createACGenSmall tokenListRestricted equivList (mkStdGen a))

expandDomain :: ACGen Token -> ACGen Token
expandDomain (ACGen domain objs rules equivs stdgen) = ACGen tokenList objs rules equivs stdgen

--testACGenExact a = addRules (addRuleReverse) 3 $ addRules (addRuleExactACGen 2 0) 10 $ addRules addRuleExactBeginACGen 2 (createACGenSmall tokenListRestricted (mkStdGen a))
--2 for begin rules seemed to result in slightly smaller rules, at least in the beginning. i think that looks good. higher numbers result in too many of the singletons being added back
    -- one change might be to not add back but only pick singletons. means we can add more small thingies but im not too worried rn
--addRuleACGen is too aggressive - needs to add on less to the existing rule i think

--for use in gephi in making force atlased graphs. looks pretty
gdfNodeMarker :: String
gdfNodeMarker = "nodedef>name VARCHAR,label VARCHAR"
gdfEdgeMarker :: String
gdfEdgeMarker = "edgedef>node1 VARCHAR,node2 VARCHAR, weight DOUBLE"

acGenToGDF :: (ShowAC a, Eq a) => ACGen a -> String
acGenToGDF = acGenToGDF' . simplifyACGen

acGenToGDF' :: (ShowAC a, Eq a) => ACGen a -> String
acGenToGDF' a@(ACGen domain objs rules equivs stdgen) = acMatrixToGDF objs (acGenToMatrix a)

acMatrixToGDF :: (ShowAC a) => [Object a] -> Matrix Int -> String
acMatrixToGDF obj m = gdfNodeMarker ++ "\n" ++ nodes ++ gdfEdgeMarker ++ "\n" ++ edges
    where nodes = concat $ map (\(a,b) -> "n"++(show a)++", "++(showAC b) ++ "\n") (zip [0..] obj)
          r = nrows m
          c = nrows m
          ind = [(x,y) | x <- [1..r], y <- [1..c]]
          edges = concat $ map (\(a,b) -> if ((m ! (a,b)) /= 0) then ("n"++(show a)++",n"++(show b)++","++(show (m ! (a,b))++"\n")) else "") ind
          --edges = concat $ map (\(a,b) -> "n"++(show a)++",n"++(show b)++","++(show (m ! (a,b))++"\n")) ind

acGenToMatrix :: (Eq a) => ACGen a -> Matrix Int
acGenToMatrix x = matrix ldab ldab (getWeight (acgen_rules x) dab)
    where dab = getUniqueObjs x
          ldab = length dab - 1

getWeight :: (Eq a) => [RecombinationRule a] -> [Object a] -> (Int, Int) -> Int
getWeight rules obj (x,y) = result
    where p1 = objectToObjectPattern (obj !! x)
          p2 = objectToObjectPattern (obj !! y)
          result = sum $ map (isIn p1 p2) rules

isIn :: (Eq a) => ObjectPattern a -> ObjectPattern a -> RecombinationRule a -> Int
isIn p1 p2 (rc, pc, _, _, _) = if (ret && ret2) then 1 else 0
    where dab = rc++pc
          ret = elem p1 dab
          ret2 = elem p2 dab

gdfExport :: IO()
gdfExport = writeFile "output.gdf" $ acGenToGDF $ testACGenExact 127
























