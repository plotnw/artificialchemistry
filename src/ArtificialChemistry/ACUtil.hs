module ArtificialChemistry.ACUtil where

import Data.List

import ArtificialChemistry.Data
import Util

replace :: Eq a => a -> a -> [a] -> [a]
replace x y = map (\z -> if z == x then y else z)

--this is a really bad way to do this, convert to string, string replace, and move back but
--
--it will have to do
replaceTokenRecombinationRule :: (ReadAC a, ShowAC a, Eq a) => a -> a -> RecombinationRule a -> RecombinationRule a
replaceTokenRecombinationRule a1 a2 lp1 = readAC $ replace (head (showAC a1)) (head (showAC a2)) (showAC lp1)

replaceTokenObject :: (ReadAC a, ShowAC a, Eq a) => a -> a -> ObjectPattern a -> ObjectPattern a
replaceTokenObject a1 a2 lp1 = readAC $ replace  (head (showAC a1)) (head (showAC a2)) (showAC lp1)

replaceTokenLine :: (ReadAC a, ShowAC a, Eq a) => a -> a -> LinePattern a -> LinePattern a
replaceTokenLine a1 a2 lp1 = readAC $ replace (head (showAC a1)) (head (showAC a2)) (showAC lp1)


--all this sum stuff is for counting the number of wildcards and stuff 
sumObject :: (Eq a) => [a] -> Object a -> ([a], [Int])
sumObject a o = (a, (foldl' combine [] counts))
    where counts = map (\x -> snd (sumLine a x)) (map snd o)

sumLine :: (Eq a) => [a] -> Line a -> ([a], [Int])
sumLine es line = (es, map (\x -> length (filter (x==) line)) es)

sumObjectPattern :: (Eq a) => [PatternElement a] -> ObjectPattern a -> ([PatternElement a], [Int], Int, Int)
sumObjectPattern es (w1, obj, w2) = (es, fst tupleCounts, snd tupleCounts, (if (w1 == Sequence) then 1 else 0) + (if (w2 == Sequence) then 1 else 0))
    where counts = map (\x -> dropTriple (sumLinePattern es x)) (map snd obj)
          tupleCounts = foldl' (\(a, x) (b, y) -> (combine a b, x+y)) ([], 0) counts

sumLinePattern :: (Eq a) => [PatternElement a] -> LinePattern a -> ([PatternElement a], [Int], Int)
sumLinePattern pes (w1, lps, w2) = (pes, map (\x -> length (filter (x==) lps)) pes, (if (w1 == Sequence) then 1 else 0) + (if (w2 == Sequence) then 1 else 0))

sumObjectPatterns :: (Eq a) => [PatternElement a] -> [ObjectPattern a] -> ([PatternElement a], [Int], Int, Int)
sumObjectPatterns pes re = foldl' combineObjectPatternResult (pes, take (length pes) (repeat 0), 0, 0) (map (sumObjectPattern pes) re)

sumReactionRule :: (Eq a) => [PatternElement a] -> RecombinationRule a -> ([PatternElement a], [Int], Int, Int)
sumReactionRule pes (re, pro, _, _, _) = foldl' combineObjectPatternResult ([], [], 0, 0) ((map (sumObjectPattern pes) re) ++ (map (sumObjectPattern pes) pro))

combineObjectPatternResult :: ([PatternElement a], [Int], Int, Int) -> ([PatternElement a], [Int], Int, Int) -> ([PatternElement a], [Int], Int, Int)
combineObjectPatternResult (pes, a, x, z) (pes', b, y, w) = (pes', combine a b, x+y, z+w)

objectToObjectPattern :: Object a -> ObjectPattern a
objectToObjectPattern a = (Null, map (\(a,b) -> (a, lineToLinePattern b)) a, Null)  

lineToLinePattern :: Line a -> LinePattern a
lineToLinePattern a = (Null, map (PatternElement) a, Null)

sizeOfObjectPattern :: ObjectPattern a -> Int
sizeOfObjectPattern (_, obj, _) = foldl' (\x (a,(b,y,c)) -> x + (length y)) 0 obj

objectPatternToObject :: ObjectPattern a -> Object a
objectPatternToObject (_, obj, _) = map (\(a,b) ->(a, linePatternToLine b)) obj 

linePatternToLine :: LinePattern a -> Line a
linePatternToLine (_, obj, _) = map patternElementToElement obj

patternElementToElement :: PatternElement a -> a
patternElementToElement (PatternElement a) = a
patternElementToElement PatternWildcard = error "cant move a wildcard to a element. check to see that you pass lists to generators without PatternWildcard in it"

containsRR :: (Eq a) => RecombinationRule a ->  a -> Bool
containsRR (p, s, _, _, _) a = if p `containsOPL` a
                                then True
                                else s `containsOPL` a

containsOPL :: (Eq a) =>  [ObjectPattern a] -> a -> Bool
containsOPL ([]) a = False
containsOPL (o:os) a = if (o `containsOP` a) then True else os `containsOPL` a

containsOP :: (Eq a) => ObjectPattern a -> a -> Bool
containsOP objp a = containsOP' ((\x -> (map snd (snd3 x))) objp) a

containsOP' :: (Eq a) => [LinePattern a] -> a -> Bool
containsOP' [] a = False
containsOP' ((_, x, _):xs) a = if elem (PatternElement a) x then True else containsOP' xs a











