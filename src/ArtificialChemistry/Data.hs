{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module ArtificialChemistry.Data where

import Data.List.Split
import Data.List
import Util

class Element a where
    (=@=) :: a -> a -> Bool

class ShowAC a where
    showAC :: a -> String

class ReadAC a where
    readAC :: String -> a

instance ShowAC Int where
    showAC = show

instance ReadAC Int where
    readAC = read

data Token = A | B | C | D | E | F | G | H | I | J | K | L | M | N | O | P | Q | R | S | T | U | V | W | X | Y | Z deriving (Show, Eq)
--data Token = W | X | Y | Z | V deriving (Show, Eq)

tokenList :: [Token]
--tokenList = [V, W, X, Y, Z]
tokenList = [A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z]

tokenListRestricted :: [Token]
tokenListRestricted = [W, X, Y, Z, V]

patternTokenList :: [PatternElement Token]
patternTokenList = map (\x -> PatternElement x) tokenList

patternTokenListRestricted :: [PatternElement Token]
patternTokenListRestricted = map (\x -> PatternElement x) tokenListRestricted

patternElementList :: [PatternElement Token]
patternElementList = PatternWildcard:patternTokenList
--patternElementList = [PatternWildcard, PatternElement V, PatternElement W, PatternElement X, PatternElement Y, PatternElement Z]

instance ShowAC Token where
    showAC = show

instance ReadAC Token where
    readAC "A" = A :: Token
    readAC "B" = B :: Token
    readAC "C" = C :: Token
    readAC "D" = D :: Token
    readAC "E" = E :: Token
    readAC "F" = F :: Token
    readAC "G" = G :: Token
    readAC "H" = H :: Token
    readAC "I" = I :: Token
    readAC "J" = J :: Token
    readAC "K" = K :: Token
    readAC "L" = L :: Token
    readAC "M" = M :: Token
    readAC "N" = N :: Token
    readAC "O" = O :: Token
    readAC "P" = P :: Token
    readAC "Q" = Q :: Token
    readAC "R" = R :: Token
    readAC "S" = S :: Token
    readAC "T" = T :: Token
    readAC "U" = U :: Token
    readAC "V" = V :: Token
    readAC "W" = W :: Token
    readAC "X" = X :: Token
    readAC "Y" = Y :: Token
    readAC "Z" = Z :: Token
    
    readAC a = error (a ++ " is not a token")

type Line a = [a]

instance {-# OVERLAPPABLE #-} (ShowAC a) => ShowAC (Line a) where
    showAC l = concat (map (showAC) l)

instance {-# OVERLAPPABLE #-} (ReadAC a) => ReadAC (Line a) where
    readAC l = map (readAC) (singletonString l)

type Object a = [(Int, Line a)]

instance {-# OVERLAPPING #-}  (ShowAC a) => ShowAC (Object a) where
    showAC x = concat $ map (\(z,y) -> showAC z ++ "#" ++ showAC y ++ "/") x

instance {-# OVERLAPPING #-} (ReadAC a) => ReadAC (Object a) where
    readAC x = map lineParse lines
        where lines = splitOn "/" (init x)
              lineParse x = (readAC(head (splitOn "#" x)), readAC (last (splitOn "#" x)))

getElementAtIndexObject :: (Eq a) => Object a -> Int -> Int -> Maybe a
getElementAtIndexObject o i j = elem
    where line = safeGet o j
          elem = if (line == Nothing) then Nothing else safeGet (snd (unsafeExtractMaybe line)) (i - (fst (unsafeExtractMaybe line))) 

getDimensionObject :: Object a -> (Int, Int)
getDimensionObject o = (maximum (map (\x -> (fst x + length (snd x))) o), length o)

data Wildcard = Null | Sequence deriving (Show, Eq)

instance ShowAC Wildcard where
    showAC Null     = "~"
    showAC Sequence = "*"

instance ReadAC Wildcard where
    readAC "~" = Null
    readAC "*" = Sequence

data PatternElement a = PatternElement a | PatternWildcard deriving (Show, Eq)

instance (ShowAC a) => ShowAC (PatternElement a) where
    showAC (PatternElement a) = showAC a
    showAC PatternWildcard    = "%"

instance (ReadAC a) => ReadAC (PatternElement a) where
    readAC x = if (x == "%") then PatternWildcard else (PatternElement (readAC x))

type Pattern a = [PatternElement a]

instance (ShowAC a) => ShowAC (Pattern a) where
    showAC = concat . map (showAC)

instance (ReadAC a) => ReadAC (Pattern a) where
    readAC x = map (readAC) (singletonString x)

--possible issue here: actually almost certainly issue
--we dont keep track of the offset for a linepattern, which is something Tominaga does
--i might forgo it since its easier not to
--but thing to keep in mind
type LinePattern a = (Wildcard, Pattern a, Wildcard)

instance (ShowAC a) => ShowAC (LinePattern a) where
    showAC (w1, p, w2) = showAC (w1) ++ showAC (p) ++ showAC (w2)

instance (ReadAC a) => ReadAC (LinePattern a) where
    readAC x = (readAC (wd1:[]), readAC mid, readAC (wd2:[]))
        where
            wd1 = head x
            mid = init (tail x)
            wd2 = last x

type ObjectPattern a = (Wildcard, [(Int, LinePattern a)], Wildcard)
--this should also be like the line pattern, in that the object pattern can match with leading and trailing lines that dont match. shouldnt be that hard to implement, its very similar to the object pattern that way
--im sure that there is a way to genericise all of these, but we will worry about that later on

instance {-# OVERLAPPING #-} (ShowAC a) => ShowAC (ObjectPattern a) where
    showAC (w1, z, w2) =
        showAC(w1) ++ "{" ++ (concat $ map (\x -> showAC (fst x) ++ "#" ++ (showAC (snd x)) ++ "/") z) ++ "}" ++ showAC(w2)

showobjRR :: (ShowAC a, Eq a) => PatternElement a -> ObjectPattern a -> String
showobjRR t (w1, z, w2) = 
        showAC(w1) ++ (concat $ map (\x -> showAC (fst x) ++ (showlinRR t (snd x)) ) z) ++ showAC(w2)

showlinRR :: (ShowAC a, Eq a) => PatternElement a -> LinePattern a -> String
showlinRR t (w1, z, w2) = showAC(w1) ++ showpatRR t z ++ showAC(w2)

showpatRR :: (ShowAC a, Eq a) => PatternElement a -> Pattern a -> String
showpatRR = showpatRR' ""

showpatRR' :: (ShowAC a, Eq a) => String -> PatternElement a -> Pattern a -> String
showpatRR' s t [] = s
showpatRR' s t (x:xs) = if (x == t) then showpatRR' s t xs else showpatRR' ((head(showAC(x))):s) t xs

instance {-# OVERLAPPING #-} (ReadAC a) => ReadAC (ObjectPattern a) where
    readAC x = (readAC [head x], map lineParse lines, readAC [last x])
        where lines = splitOn "/" (init (init (init (tail (tail x)))))
              lineParse x = (readAC(head (splitOn "#" x)), readAC (last (splitOn "#" x)))

 --where stripI = filter (not . (`elem` " +{}=>")) x
        
type RecombinationRule a
    = ([ObjectPattern a], [ObjectPattern a], [Int], [Int], [Int])
    --three int list functions, for where to map singlton wildcards, line ending wildcards, and object ending wildcards, respectively.

instance (ShowAC a) => ShowAC (RecombinationRule a) where
    showAC (r, p, f, g, h) =
        (concat (intersperse " + " (map showAC r))) ++ " => " ++ (concat (intersperse " + " (map showAC p))) ++ " | " ++ show f ++ " | " ++ show g ++ " | " ++ show h

showRR :: (ShowAC a, Eq a) => PatternElement a -> RecombinationRule a -> String
showRR t (r,p,f,g,h) = concat (map (showobjRR t) r) ++ concat (map (showobjRR t) p)

instance (ReadAC a) => ReadAC (RecombinationRule a) where
    readAC x = (reactants, products, id, id2, id3)
        where basex = filter (\y -> not (y `elem` "> ")) x
              splitted = splitOn "|" basex
              id    = read $ splitted !! 1
              id2   = read $ splitted !! 2
              id3   = read $ splitted !! 3
              products = map (readAC) (splitOn "+" (last (splitOn "=" (head splitted))))
              reactants = map (readAC) (splitOn "+" (head (splitOn "=" (head splitted))))

data ArtificialChemistry a = ArtificialChemistry {universe :: [Object a], rules :: [RecombinationRule a], sources :: [ObjectPattern a], drains :: [ObjectPattern a]} deriving (Show, Eq)



main :: IO ()
main = do
    let
        a =
            (( Null
             , [PatternElement W, PatternElement W, PatternElement W]
             , Sequence
             )
            )
    print $ showAC a
    let
        b =
            (( Null
             , [PatternElement W, PatternElement Z, PatternElement Z]
             , Sequence
             )
            )
    let
        c =
            (( Null
             , [PatternElement X, PatternElement Z, PatternWildcard]
             , Sequence
             )
            )
    let dab  = [(0 :: Int, a), (1 :: Int, b)]
    let yote = [(0 :: Int, c)]
    let yeet = ([(Null, dab, Null), (Null, dab, Null)], [(Sequence, yote, Null)], [2,1] :: [Int], [] :: [Int], [] :: [Int])
    print (showAC yeet)
    print (show yeet)
