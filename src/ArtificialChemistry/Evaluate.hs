module ArtificialChemistry.Evaluate where


import Data.Array
import Data.List

import ArtificialChemistry.Data
import ArtificialChemistry.ACUtil
import Util

--https://wiki.haskell.org/Edit_distance
--need to figure out how it works, or test library for this
lev''' :: Eq a => [a] -> [a] -> Int
lev''' a b = last
    (if lab == 0
        then mainDiag
        else if lab > 0
            then lowers !! (lab - 1)
            else{- < 0 -}
                 uppers !! (-1 - lab)
    )
  where
    mainDiag = oneDiag a b (head uppers) (-1 : head lowers)
    uppers   = eachDiag a b (mainDiag : uppers) -- upper diagonals
    lowers   = eachDiag b a (mainDiag : lowers) -- lower diagonals
    eachDiag a [] diags = []
    eachDiag a (bch : bs) (lastDiag : diags) =
        oneDiag a bs nextDiag lastDiag : eachDiag a bs diags
        where nextDiag = head (tail diags)
    oneDiag a b diagAbove diagBelow = thisdiag
      where
        doDiag [] b  nw n w = []
        doDiag a  [] nw n w = []
        doDiag (ach : as) (bch : bs) nw n w =
            me : (doDiag as bs me (tail n) (tail w))
            where me = if ach == bch then nw else 1 + min3 (head w) nw (head n)
        firstelt = 1 + head diagBelow
        thisdiag = firstelt : doDiag a b firstelt diagAbove (tail diagBelow)
    lab = length a - length b
    min3 x y z = if x < y then x else min y z

--https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance
--Source from https://www.reddit.com/r/programming/comments/w4gs6/levenshtein_distance_in_haskell/c5a6jjz/
    {-lev :: (Eq a) => [a] -> [a] -> Int
lev [] [] = 0
lev [] ys = length ys
lev xs [] = length xs
lev (x:xs) (y:ys) = if (x == y) then lev xs ys else (minimum [lev xs (y:ys), lev (x:xs) ys, lev xs ys])

lev''' :: (Eq a) => [a] -> [a] -> Int
lev''' xs ys = levMemo ! (n, m)
  where levMemo = array ((0,0),(n,m)) [((i,j),lev i j) | i <- [0..n], j <- [0..m]]
        n = length xs
        m = length ys
        xa = listArray (1, n) xs
        ya = listArray (1, m) ys
        lev 0 v = v
        lev u 0 = u
        lev u v
          | xa ! u == ya ! v = levMemo ! (u-1, v-1)
          | otherwise        = 1 + minimum [levMemo ! (u, v-1),
                                            levMemo ! (u-1, v),
                                            levMemo ! (u-1, v-1)] -}
getDistanceLinePattern :: (ShowAC a, Eq a) => LinePattern a -> LinePattern a -> Int
getDistanceLinePattern lp1 lp2 = lev''' (showAC lp1) (showAC lp2)

getDistanceObjectPattern :: (ShowAC a, Eq a) => ObjectPattern a -> ObjectPattern a -> Int
getDistanceObjectPattern op1 op2 = lev''' (showAC op1) (showAC op2)

--getDistanceRecombinationRule :: (ShowAC a, Eq a) => RecombinationRule a -> RecombinationRule a -> Int
--getDistanceRecombinationRule rr1 rr2 = lev''' (showRR rr1) (showRR rr2)

compareTokenArtificialChemistry :: (ReadAC a, Eq a, ShowAC a) => PatternElement a -> PatternElement a -> ArtificialChemistry a -> Double
compareTokenArtificialChemistry t1 t2 ac = (fromIntegral (sum ruleDistances)) / (fromIntegral (length ruleDistances))
    where ruleDistances = map (compareTokenRecombinationRule t1 t2) (rules ac)

compareTokenRules :: (ReadAC a, Eq a, ShowAC a) => PatternElement a -> PatternElement a -> [RecombinationRule a] -> Double
compareTokenRules t1 t2 rrs = (fromIntegral (sum ruleDistances)) / (fromIntegral (length ruleDistances))
    where ruleDistances = map (compareTokenRecombinationRule t1 t2) rrs

--DONT PASS WILDCARD PATTERNELEMENT TO THIS
compareTokensRules :: (ReadAC a, Eq a, ShowAC a) => [PatternElement a] -> [RecombinationRule a] -> Double
compareTokensRules l rrs = average results
    where uniques = pairs l
          results = map (\(x,y) -> compareTokenRules x y rrs) uniques

compareTokenRecombinationRule :: (ReadAC a, Eq a, ShowAC a) => PatternElement a -> PatternElement a -> RecombinationRule a -> Int
compareTokenRecombinationRule t1 t2 rr = lev''' (showRR t1 rr) (showRR t2 rr)
    
    --getDistanceRecombinationRule (showRR t1 rr (replaceTokenRecombinationRule t1 t2 rr)

conservationOfMatterEvaluation :: (Eq a) => [PatternElement a] -> RecombinationRule a -> Double
conservationOfMatterEvaluation basis rule@(r, p, _, _, _) = 1.0 - result
    where resultR = sumObjectPatterns basis r
          resultP = sumObjectPatterns basis p
          summedS = combine (snd4 resultR) (snd4 resultP)
          diffedS = map abs (zipWith (-) (snd4 resultR) (snd4 resultP))
          result  = (0.0 + fromIntegral(foldl' (+) 0 diffedS)) / (fromIntegral(foldl' (+) 0 summedS))

conservationOfWildcardEvaluation :: (Eq a) => [PatternElement a] -> RecombinationRule a -> Double
conservationOfWildcardEvaluation basis rule@(r, p, _, _, _) = 1.0 - result
    where resultR = sumObjectPatterns basis r
          resultP = sumObjectPatterns basis p
          o = fromIntegral (fth4 resultR - fth4 resultP) / fromIntegral (fth4 resultR + fth4 resultP)
          l = fromIntegral (trd4 resultR - trd4 resultP) / fromIntegral (trd4 resultR + trd4 resultP)
          result = (abs(o)+ abs(l)) / 2
          
 
