module ArtificialChemistry.Examples where

import ArtificialChemistry.Data
import ArtificialChemistry.PatternMatching


lineWZX :: Line Token
lineWZX = readAC "WZX"

lineXZXWWW :: Line Token
lineXZXWWW = readAC "XZXWWW"

lineWWW :: Line Token
lineWWW = readAC "WWW"

lineZZX :: Line Token
lineZZX = readAC "ZZX"

object :: Object Token
object = readAC "0#WWWW/2#ZXZ/"

object2 :: Object Token
object2 = readAC "0#WWWZW/0#ZWZZXXX/"

object3 :: Object Token
object3 = readAC "0#WWZZW/3#ZWZXX/2#WX/"

object4 :: Object Token
object4 = readAC "0#WWZ/2#X/"

objectPattern :: ObjectPattern Token
objectPattern = readAC "~{0#~%WZ~/2#~X~/}~"

linePatternNWWA :: LinePattern Token
linePatternNWWA = readAC "*WWW~"

linePatternNW_WA :: LinePattern Token
linePatternNW_WA = readAC "~W%W*"

objectPattern1 :: ObjectPattern Token
objectPattern1 = readAC "*{0#~WWW*/1#*WZX*/}*"

objectPattern2 :: ObjectPattern Token
objectPattern2 = readAC "~{0#~WWW*/1#*WZZ*/}~"

objectPatternBigP :: ObjectPattern Token
objectPatternBigP = readAC "~{0#*Z*/0#~Z*/0#*X~/}~"

rr :: RecombinationRule Token
rr = readAC "~{0#~WWW*/1#*WZX*/}* + ~{0#*Z*/0#~Z*/0#*X~/}~ => ~{0#~WWW*/1#*WZZ*/}~ | [] | [] | []"

rr2 :: RecombinationRule Token
rr2 = readAC "~{0#~XXX*/1#*XZW*/}* + ~{0#*Z*/0#~Z*/0#*X~/}~ => ~{0#~XXX*/1#*XZZ*/}~ | [] | [] | []"

--really basic examples for testing

objectW :: Object Token
objectW = readAC "0#W/"

objectX :: Object Token
objectX = readAC "0#X/"

objectXXW :: Object Token
objectXXW = readAC "0#VXXW/"

objectZWY :: Object Token
objectZWY = readAC "0#ZWY/"

objp1 :: ObjectPattern Token
objp1 = readAC "~{0#~%~/}~"

objp2 :: ObjectPattern Token
objp2 = readAC "~{0#~XXW~/}~"

objp3 :: ObjectPattern Token
objp3 = readAC "~{0#~WXXW~/}~"

objpP :: ObjectPattern Token
objpP = readAC "~{0#*X%*/}~"

-- testRR routine
-- :r
-- let dab = head $ fst $ getPossibleRecombinationRule objlist rrBasic
-- applyRecombinationRule objlist dab rr
-- --observe the carnage

objlist = [objectW, objectX, objectXXW, objectZWY]

rrBasic :: RecombinationRule Token
rrBasic = readAC "~{0#~%*/}~ + ~{0#*X%*/}~ => ~{0#*%XXW~/}~ + ~{0#*%X*/}~ | [1,0] | [1,0,2] | []"

rrMan :: RecombinationRule Token
rrMan = ([objp1, objp2], [objp3], [], [], [])


b = snd $ head [(0, linePatternNW_WA)]
a = snd $ head object

artificialChemistry :: ArtificialChemistry Token
artificialChemistry = ArtificialChemistry [object, object2, object3] [rr,rr2] [] []
--Random Testing Stuff
dab =  matchObject objectPattern1 object3

pls = map objectMatchToObject dab

output = map showAC pls
