module ArtificialChemistry.PatternMatching where

import ArtificialChemistry.Data
import Util

import Data.List
import Data.List.Extra


--pattern, list of wildcards from left to right
type PatternMatch a = (Pattern a, [a])

matchPattern :: (Eq a) => Pattern a -> Line a -> Maybe (PatternMatch a)
matchPattern p l = if (wilds == Nothing) then Nothing else wilds >>= (\x -> (Just (p, x)))
    where wilds = matchPattern' [] p l

matchPattern' :: (Eq a) => [a] -> Pattern a -> Line a -> Maybe [a]
matchPattern' w ([]) ([]) = Just w
matchPattern' w ([]) (x:xs) = Nothing
matchPattern' w (a:as) ([]) = Nothing
matchPattern' w (PatternWildcard:as) (x:xs) = matchPattern' (x:w) as xs
matchPattern' w ((PatternElement a):as) (x:xs) = if (a == x) then matchPattern' w as xs else Nothing

type LineMatch a = (Pattern a, [a], Line a, Line a, Int)
--Pattern, wildcards, part of line to left, part of line to right of match, offset from beginning of line
--more related to the line match, which is just this but stricter on the line endings it can accept

lineMatchToLine :: (Eq a) => LineMatch a -> Line a
lineMatchToLine (pat, wild, l, r, off) = l ++ (patternFill pat wild) ++ r

lineMatchToObjectLine :: (Eq a) => LineMatch a -> (Int, Line a)
lineMatchToObjectLine mat@(pat, wild, l, r, off) = (off-length(l), lineMatchToLine mat)
patternFill :: (Eq a) => Pattern a -> [a] -> Line a
patternFill = patternFill' [] .  reverse

patternFill' :: (Eq a) => Line a -> Pattern a -> [a] -> Line a
patternFill' a ([]) _ = a
patternFill' a (PatternWildcard:xs) (w:ws) = patternFill' (w:a) xs ws
patternFill' a ((PatternElement b):xs) (ws) = patternFill'(b:a) xs ws


cSubPatternMatch :: Maybe (PatternMatch a) -> Line a -> Line a -> Maybe (LineMatch a)
cSubPatternMatch Nothing _ _ = Nothing
cSubPatternMatch (Just (p, x)) init tail = Just (p, x, init, tail, length (init))

matchLine' :: (Eq a) => Pattern a -> Line a -> [LineMatch a]
matchLine' p l = map unsafeExtractMaybe $ filter ((/=) Nothing) all
    where all = map (\(mpm, init, tail) -> (cSubPatternMatch mpm init tail)) dab
          initsl = inits l
          tailsl = map (drop (length p)) (tails l)
          swp = map (matchPattern p) (map (take (length p)) (tails l))
          dab = zip3 swp initsl tailsl 

matchLine :: (Eq a) => LinePattern a -> Line a -> [LineMatch a]
matchLine (Null, p, Null) l = filter (\(x,y,z,w,q) -> (z==[] && w==[])) $ matchLine' p l
matchLine (Null, p, Sequence) l = filter (\(x,y,z,w,q) -> (z==[] && w/=[])) $ matchLine' p l
matchLine (Sequence, p, Null) l = filter (\(x,y,z,w,q) -> (z/=[] && w==[])) $ matchLine' p l
matchLine (Sequence, p, Sequence) l = filter (\(x,y,z,w,q) -> (z/=[] && w/=[])) $ matchLine' p l

type ObjectMatch a = ([LineMatch a], Object a, Object a)


--ok current issue with this is that the object match doesnt keep track of line offsets
--our option is to just carry over the one form the initial object, but doingit that way means
--we ignore patterns being connected across lines, which is kindof a big deal actually
--yeah so for that we need to make linematching take offset into account
--alternatively we can check for that in object matching, which i like a bit better
--would check for length of wildcard, if any + starting offset of line equals the pattern offset, which I think we have in the type rn but i have to check
--yeah thats pretty easy, just an extra if to check prolly in the object. thats fine, we have to rewrite
--some things but the logic will stay
--cool
objectMatchToObject :: (Eq a) => ObjectMatch a -> Object a
objectMatchToObject (m, l, r) = l ++ (map lineMatchToObjectLine m) ++ r

{-
matchObject :: (Eq a) => ObjectPattern a -> Object a -> [ObjectMatch a]
matchObject p o = map unsafeExtractMaybe $ filter ((/=) Nothing) all
    where all = dab
          initsl = inits o
          tailsl = map (drop (length p)) (tails l)
          swp = map (matchPattern p) (map (take (length p)) (tails l))
-}

--im scared to debug this since i know there will be a bug and i know it wont work
--pls work
matchObject :: (Eq a) => ObjectPattern a -> Object a -> [ObjectMatch a]
matchObject (Null, obp, Null) o = filter (\(x,y,z) -> (y == [] && z == [])) (matchObject''' obp o)
matchObject (Sequence, obp, Null) o = filter (\(x,y,z) -> (y /= [] && z == [])) (matchObject''' obp o)
matchObject (Null, obp, Sequence) o = filter (\(x,y,z) -> (y == [] && z /= [])) (matchObject''' obp o)
matchObject (Sequence, obp, Sequence) o = filter (\(x,y,z) -> (y /= [] && z /= [])) (matchObject''' obp o)

--returns all matches, ignoring wildcards
--map matchObject'' over tails, combine the outputs of that with the corresponding inits for the object, then pass upwards to matchObject
--matchObject sorts pretty much identically to matchLine


matchObject''' :: (Eq a) => [(Int, LinePattern a)] -> Object a -> [ObjectMatch a]
matchObject''' y o = concat finaliedLists
    where is = inits o
          ts = tails o
          finaliedLists = map (\(i, t) -> addObjectToHalfMatch i (matchObject'' y t)) (zip is ts)



matchObject' :: (Eq a) => [[LineMatch a]] -> [(Int, LinePattern a)] -> Object a -> Maybe (([[LineMatch a]], Object a))
matchObject' w ([]) ([]) = Just (w, [])
matchObject' w ([]) (x:xs) = Just (w, x:xs)
matchObject' w (a:as) ([]) = Nothing
--we will assume that there isnt anything weird with the line pattern, like an offset, but null first wildcard
--need to account for offsets - currently it does none of that
--couple options - do the first one seperately and add to the offset of each in remaining list?
    --kinda like that ngl
matchObject' w (p:ps) (l:ls) =  if ([] == goodMatchLines) then Nothing else matchObject' (w ++ [goodMatchLines]) ps ls
    where matchedLines = matchLine (snd p) (snd l)
          goodMatchLines = filter (\(x,y,z,w,q) -> q + (fst l) == fst p) matchedLines

addObjectToHalfMatch :: (Eq a) => Object a -> [([LineMatch a], Object a)] -> [ObjectMatch a]
addObjectToHalfMatch obj hell = map (\(y, z) -> (y, obj, z)) hell
    {-where heck = map (\(x,y) -> map (\z -> (z, y)) x) hell
          heck1 = map fst heck
          heck2 = map snd heck-}
--note the first thingie here is the middle of an object pattern
--why is it [[LineMatch a]]? because its like the lists of matches on each line or something? i thought sequences ooooo since there are multiple possible matches as you go along i guess. so we need to map over each one inside at the end 
matchObject'' :: (Eq a) => [(Int, LinePattern a)] -> Object a -> [([LineMatch a], Object a)]
matchObject'' [] _ = []
matchObject'' _ [] = []
--thusd we know an element exists
--will want to cartesian product all of the sublists, so if you get 1 match for first, 3 for second, 2 for third line, you get 6 total matches. this is done by the sequence later on
matchObject'' (p:ps) (o:os) = if (matcheswithObject == []) then [] else zip objectPatterns (repeat (snd (head (matcheswithObject))))
    where firstLineMatches = matchLine (snd p) (snd o)
          laterMatches = map (\who@(x,y,z,w,q) -> matchObject' [[who]] (offsetObjectPattern q ps) os) firstLineMatches
          matcheswithObject= (map unsafeExtractMaybe (filter (/= Nothing) laterMatches))
          matchesOnly = map fst matcheswithObject
          objectPatterns = map concat $ map sequence matchesOnly
--get first match - for each possible match, try the next part with the offset. then combine them back together to make linematch i guess, checking fro nothing as returning nothing. another functor operation


--helper thingie for adjusting offset given a first element
offsetObjectPattern :: Int -> [(Int, LinePattern a)] -> [(Int, LinePattern a)]
offsetObjectPattern offset patterns = map (\(x,y) -> (x+offset, y)) patterns 

type RecombinationRuleMatch a = ([[ObjectMatch a]], RecombinationRule a)

getPossibleRecombinationRule :: (Eq a) => [Object a] -> RecombinationRule a -> RecombinationRuleMatch a
getPossibleRecombinationRule objs rr@(rea, pro, f, g, h) = ((getObjectMatchesForRR objs rea), rr)

getObjectMatchesForRR :: (Eq a) => [Object a] -> [ObjectPattern a] -> [[ObjectMatch a]]
getObjectMatchesForRR objs objps = indexesToObjectMatch objs objps (getIndexesOfMatches objs objps)

indexesToObjectMatch :: (Eq a) => [Object a] -> [ObjectPattern a] -> [[Int]] -> [[ObjectMatch a]]
indexesToObjectMatch = indexesToObjectMatch' []

indexesToObjectMatch' :: (Eq a) => [[ObjectMatch a]] -> [Object a] -> [ObjectPattern a] -> [[Int]] -> [[ObjectMatch a]]
indexesToObjectMatch' bad _ ([]) _ = bad
indexesToObjectMatch' bad _ _ ([]) = bad
indexesToObjectMatch' bad objs objps@(op:ops) (i:is) = indexesToObjectMatch' (newbad:bad) objs ops is
    where newbad = concat $ sequence (zipWith (\x y -> matchObject x (objs !! y)) objps i)
      
    --where newbad = concat $ sequence (map (\x -> matchObject op (objs !! x)) i)

getIndexesOfMatches :: (Eq a) => [Object a] -> [ObjectPattern a]-> [[Int]]
getIndexesOfMatches os ops = filter (isBad) results
    where results = sequence (map (getMatches os) ops)

isBad :: [Int] -> Bool
isBad = not . anySame

getMatches :: (Eq a) => [Object a] -> ObjectPattern a -> [Int]
getMatches = getMatches' 0 []

getMatches' :: (Eq a) => Int -> [Int] -> [Object a] -> ObjectPattern a -> [Int]
getMatches' i is ([]) op = is
getMatches' i is (o:os) op = getMatches' (i+1) ((if (matchObject op o /= []) then [i] else []) ++ is) os op


applyRecombinationRule :: (Eq a) => [Object a] -> [ObjectMatch a] -> RecombinationRule a -> [Object a]
applyRecombinationRule objs matches (a, b, f, g, h) = (map objectMatchToObject matched) ++ (objs \\ (map objectMatchToObject matches))
    where extracted = extractWildcardsObjects matches
          matched = fillObjectPatterns b ((shuffle (fst3 extracted) f), (shuffle (snd3 extracted) g), (shuffle (trd3 extracted) h))

fillObjectPatterns :: (Eq a) => [ObjectPattern a] -> ([a], [Line a], [Object a]) -> [ObjectMatch a]
fillObjectPatterns = fillObjectPatterns' []

fillObjectPatterns' :: (Eq a) => [ObjectMatch a] -> [ObjectPattern a] -> ([a], [Line a], [Object a]) -> [ObjectMatch a]
fillObjectPatterns' a ([]) _ = a
fillObjectPatterns' a (x:xs) fs@(ea, la, oa) = fillObjectPatterns' (a ++ [fst result]) xs (snd result)
    where result = fillObjectPattern x fs

fillObjectPattern :: (Eq a) => ObjectPattern a -> ([a], [Line a], [Object a]) -> (ObjectMatch a, ([a], [Line a], [Object a]))
fillObjectPattern (Null, a, Null) (f,g,h) = ((fst result, [], []), (fst (snd result), snd (snd result), h))
    where result = fillLinePatterns (f,g) a
fillObjectPattern (Null, a, Sequence) (f,g,h) = ((fst result, [], head h), (fst (snd result), snd (snd result), tail h))
    where result = fillLinePatterns (f,g) a
fillObjectPattern (Sequence, a, Null) (f,g,h) = ((fst result, head h, []), (fst (snd result), snd (snd result), tail h))
    where result = fillLinePatterns (f,g) a
fillObjectPattern (Sequence, a, Sequence) (f,g,h) = ((fst result, h !! 0, h !! 1), (fst (snd result), snd (snd result), drop 2 h))
    where result = fillLinePatterns (f,g) a

fillLinePatterns :: (Eq a) => ([a], [Line a]) -> [(Int, LinePattern a)] -> ([LineMatch a], ([a], [Line a]))
fillLinePatterns = fillLinePatterns' []

fillLinePatterns' :: (Eq a) => [LineMatch a] -> ([a], [Line a]) -> [(Int, LinePattern a)] -> ([LineMatch a], ([a], [Line a]))
fillLinePatterns' a b ([]) = (a,b)
fillLinePatterns' b c (x:xs) = fillLinePatterns' (b ++ [fst result]) (snd result) xs
    where result = fillLinePattern x c


fillLinePattern :: (Eq a) => (Int, LinePattern a) -> ([a], [Line a]) -> (LineMatch a, ([a], [Line a]))
fillLinePattern (x, (Null, a, Null)) (ea, la) = ((a, snd (fst filled), [], [], x), (snd filled, la))
    where filled = fillPattern a ea
fillLinePattern (x, (Sequence, a, Null)) (ea, la) = ((a, snd (fst filled), head la, [], x), (snd filled, tail la))
    where filled = fillPattern a ea
fillLinePattern (x, (Null, a, Sequence)) (ea, la) = ((a, snd (fst filled), [], head la, x), (snd filled, tail la))
    where filled = fillPattern a ea
fillLinePattern (x, (Sequence, a, Sequence)) (ea, la) = ((a, snd (fst filled), la !! 0, la !! 1, x), (snd filled, drop 2 la))
    where filled = fillPattern a ea

fillPattern :: (Eq a) => Pattern a -> [a] -> (PatternMatch a, [a])
fillPattern a b = ((a, take dab b), drop dab b)
    where dab = length (filter (PatternWildcard==) a)

extractWildcardsObjects :: [ObjectMatch a] -> ([a], [Line a], [Object a])
extractWildcardsObjects = extractWildcardsObjects' ([], [], [])

extractWildcardsObjects' :: ([a], [Line a], [Object a]) -> [ObjectMatch a] -> ([a], [Line a], [Object a])
extractWildcardsObjects' ret ([]) = ret
extractWildcardsObjects' (a, b, c) ((lines, obj1, obj2):xs) = extractWildcardsObjects' (a ++ fst dab, b ++ snd dab, c ++ ale obj1 ++ ale obj2) xs
    where dab = extractWildcardsLines lines 

extractWildcardsLines :: [LineMatch a] -> ([a], [Line a])
extractWildcardsLines = extractWildcardsLines' ([], [])

extractWildcardsLines' :: ([a], [Line a]) -> [LineMatch a] -> ([a], [Line a])
extractWildcardsLines' ret ([]) = ret
extractWildcardsLines' (a,b) ((_, wilds, line1, line2, _):xs) = extractWildcardsLines' (a ++ wilds, b ++ ale line1 ++ ale line2) xs

ale :: [a] -> [[a]]
ale ([]) = []
ale x = [x]











































