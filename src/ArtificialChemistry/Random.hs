module ArtificialChemistry.Random where

import System.Random

import ArtificialChemistry.Data hiding (main)
import ArtificialChemistry.ACUtil
import Util

maxSizeLine :: Int
maxSizeLine = 5

minSizeLine :: Int
minSizeLine = 1

--tokenList :: [Token]
--tokenList = [A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z] 

wildcardList :: [Wildcard]
wildcardList = [Null, Sequence]

generateRandomLine :: (Eq a) => (Int -> a) -> StdGen -> Line a
generateRandomLine f rg = line
    where listR = randoms rg
          lineLength = minSizeLine + ((head listR) `mod` (maxSizeLine-minSizeLine))
          line = map f (take lineLength listR)

generateRandomLinePattern :: (Eq a) => (Int -> PatternElement a) -> StdGen -> LinePattern a
generateRandomLinePattern f rg = (w1, line, w2)
    where listR = randoms rg
          w1 = intToListElement wildcardList (listR !! 0) 
          w2 = intToListElement wildcardList (listR !! 1)
          line = generateRandomLine f (mkStdGen (listR !! 2))

maxSizeOffset :: Int
maxSizeOffset = 4

minSizeOffset :: Int
minSizeOffset = 1

maxLengthObject :: Int
maxLengthObject = 5

minLengthObject :: Int
minLengthObject = 1

generateRandomObject :: (Eq a) => (Int -> a) -> StdGen -> Object a
generateRandomObject f r = generateRandomObject' objLength [(0, line)] f (mkStdGen (head (randoms r)))
    where listR = tail $ randoms r
          line = generateRandomLine f (mkStdGen (listR !! 0))
          objLength = minLengthObject + ((listR !! 1) `mod` (maxLengthObject-minLengthObject))

generateRandomObject' :: (Eq a) => Int -> [(Int, Line a)] -> (Int -> a) -> StdGen -> [(Int, Line a)]
generateRandomObject' 0 a f r = (fixGeneratedObject a)
generateRandomObject' n a f r = generateRandomObject' (n-1) (newline:a) f (mkStdGen (head (randoms r)))
    where listR = tail $ randoms r
          newLine = generateRandomLine f (mkStdGen (listR !! 0))
          minSpot = (fst (head a)) - length (newLine) + 1
          maxSpot = (fst (head a) + length (snd (head a)))
          lineSpot = minSpot + (listR !! 1) `mod` (maxSpot - minSpot)
          newline = (lineSpot, newLine)

generateRandomObjectPattern :: (Eq a) => (Int -> PatternElement a) -> StdGen -> ObjectPattern a
generateRandomObjectPattern f r = (w1, generateRandomObjectPattern' objLength [(0, line)] f (mkStdGen (head (randoms r))), w2)
    where listR = tail $ randoms r
          line = generateRandomLinePattern f (mkStdGen (listR !! 0))
          objLength = minLengthObject + ((listR !! 1) `mod` (maxLengthObject-minLengthObject))
          w1 = intToListElement wildcardList (listR !! 2) 
          w2 = intToListElement wildcardList (listR !! 3)

generateRandomObjectPattern' :: (Eq a) => Int -> [(Int, LinePattern a)] -> (Int -> PatternElement a) -> StdGen -> [(Int, LinePattern a)]
generateRandomObjectPattern' 0 a f r = (fixGeneratedObject a)
generateRandomObjectPattern' n a f r = generateRandomObjectPattern' (n-1) (newline:a) f (mkStdGen (head (randoms r)))
    where listR = tail $ randoms r
          newLine = generateRandomLinePattern f (mkStdGen (listR !! 0))
          minSpot = (fst (head a)) - length (snd3 (newLine)) + 1
          maxSpot = (fst (head a) + length (snd3 (snd(head a))))
          lineSpot = minSpot + (listR !! 1) `mod` (maxSpot - minSpot)
          newline = (lineSpot, newLine)

fixGeneratedObject :: [(Int, a)]-> [(Int, a)]
fixGeneratedObject a = map (\(x,y) -> (x+offset, y)) a
    where offset = -1 * (minimum (map (\(x,y) -> x) a))

minObjectRR :: Int
minObjectRR = 1

maxObjectRR :: Int
maxObjectRR = 5

generateRandomObjectPatterns :: (Eq a) => (Int -> PatternElement a) -> StdGen -> [ObjectPattern a]
generateRandomObjectPatterns f stdgen = yeet
    where bad = randoms stdgen
          num = minObjectRR + ( (head bad) `mod` maxObjectRR)
          yeet = take num (map (\x -> generateRandomObjectPattern f (mkStdGen x)) (tail bad))

generateRecombinationRuleMinusFunctions :: (Eq a) => (Int -> PatternElement a) -> StdGen -> RecombinationRule a
generateRecombinationRuleMinusFunctions f stdgen = (r, p, [], [], [])
    where bad = randoms stdgen
          r = generateRandomObjectPatterns f (mkStdGen (bad !! 0))
          p = generateRandomObjectPatterns f (mkStdGen (bad !! 1))

addFunctionsToRecombinationRule :: (Eq a) => StdGen -> [PatternElement a] -> RecombinationRule a -> RecombinationRule a
addFunctionsToRecombinationRule stdgen list (r, p, _, _, _) = (r, p, gen1, gen2, gen3)
    where bad = randoms stdgen
          result1 = sumObjectPatterns list r
          result2 = sumObjectPatterns list p
          gen1 = generateWildcardMapping (mkStdGen (bad !! 0)) (head (snd4 result1)) (head (snd4 result2))
          gen2 = generateWildcardMapping (mkStdGen (bad !! 1)) (trd4 result1) (trd4 result2)
          gen3 = generateWildcardMapping (mkStdGen (bad !! 2)) (fth4 result1) (fth4 result2)



generateRecombinationRule :: (Eq a) => [PatternElement a] -> StdGen -> RecombinationRule a
generateRecombinationRule a stdgen = addFunctionsToRecombinationRule gen2 a (generateRecombinationRuleMinusFunctions (intToListElement a) gen1)
    where bad = randoms stdgen
          gen1 = mkStdGen $ bad !! 0
          gen2 = mkStdGen $ bad !! 1


generateWildcardMapping :: StdGen -> Int -> Int -> [Int]
generateWildcardMapping stdgen 0 _ = []
generateWildcardMapping stdgen range length = result
    where bad = randoms stdgen
          result = map (\x -> x `mod` range) (take length bad)


main :: IO()
main = do
        g <- newStdGen
        --print $ showAC $ generateRandomLine (intToListElement tokenList) g
        print $ showAC $ generateRandomObject (intToListElement tokenList) g
        --print $ showAC $ generateRandomObjectPattern (intToListElement patternElementList) g
        --print $ showAC $ generateRandomObjectPatterns (intToListElement patternElementList) g
        --print $ showAC $ generateRecombinationRuleMinusFunctions (intToListElement patternElementList) g
        --print $ showAC $ generateRecombinationRule patternElementList g



