module ArtificialChemistry.RandomEvaluation where

import ArtificialChemistry.Evaluate
import ArtificialChemistry.Random hiding (main)
import ArtificialChemistry.Data hiding (main)
import System.Random

main :: IO()
main = do
        g <- newStdGen
        let rule = generateRecombinationRule patternElementList g
        print rule
        print $ conservationOfMatterEvaluation patternElementList rule


bashConservation :: IO()
bashConservation = do
                    g <- newStdGen
                    let dab = map (mkStdGen) (randoms g)
                    mapM_ (\x -> (appendFile "data.csv" (show x)) >> appendFile "data.csv" ",\n") (take 10000 (map (\x -> conservationOfMatterEvaluation patternElementList (generateRecombinationRule patternElementList x)) dab))
