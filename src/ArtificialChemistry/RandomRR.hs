module ArtificialChemistry.RandomRR where


import System.Random
import ArtificialChemistry.Data hiding (main)
import ArtificialChemistry.ACUtil
import Util
import Unsort
import ArtificialChemistry.Evaluate
import Data.List

minTokenLength :: Int
minTokenLength = 10

maxTokenLength :: Int
maxTokenLength = 50 -- change as needed

generateList :: (Int -> PatternElement a) -> StdGen -> [PatternElement a]
generateList f stdgen = ret
    where bad = randoms stdgen
          length = minTokenLength + (bad !! 1 `mod`(maxTokenLength - minTokenLength))
          ret = map f (take length (drop 2 bad))

testGenerateList :: IO String
testGenerateList = newStdGen >>= (\x -> return (generateList (intToListElement patternElementList) x)) >>= return . showAC

shuffleList :: StdGen -> [PatternElement a] -> [PatternElement a]
shuffleList = unsort

testShuffleList :: IO ()
testShuffleList = do
    g <- newStdGen
    let b = generateList (intToListElement patternElementList) g
    print . showAC $ b
    d <- newStdGen
    print . showAC $ shuffleList d b

objAvgSplitSize :: Int
objAvgSplitSize = 10

objMaxSplitVary :: Int
objMaxSplitVary = 4

splitListObject :: StdGen -> [PatternElement a] -> [[PatternElement a]]
splitListObject = splitList objAvgSplitSize objMaxSplitVary

lineAvgSplitSize :: Int
lineAvgSplitSize = 4

lineMaxSplitVary :: Int
lineMaxSplitVary = 3

splitListLine :: StdGen -> [PatternElement a] -> [[PatternElement a]]
splitListLine = splitList lineAvgSplitSize lineMaxSplitVary

splitList :: Int -> Int -> StdGen -> [PatternElement a] -> [[PatternElement a]]
splitList = splitList' []

splitList' :: [[PatternElement a]] -> Int -> Int -> StdGen -> [PatternElement a] -> [[PatternElement a]]
splitList' x a m d ([]) = x
splitList' x a m d b = splitList' ((fst merp):x) a m nd (snd merp)
    where bad = randoms d
          nd = mkStdGen (head bad)
          takel = a + (bad !! 1 `mod` (2*m)) - m
          merp = splitAt takel b

testSplitList :: IO()
testSplitList = do
    m <- newStdGen >>= (\x -> return (generateList (intToListElement patternElementList) x)) 
    print . showAC $ m
    stdgen <- newStdGen
    mapM_ (print . showAC ) $ splitList objAvgSplitSize objMaxSplitVary stdgen m

makeLineSplits :: StdGen -> [PatternElement a] -> [[[PatternElement a]]]
makeLineSplits stdgen list = ret
    where bad = randoms stdgen
          --list = generateList f (mkStdGen (bad !! 0))
          ret = zipWith (\x y -> splitListLine y x) (splitListObject (mkStdGen (bad !! 1)) list) (map mkStdGen (drop 2 bad))

--testMakeLineSplits :: IO String -- this doesnt really work
--testMakeLineSplits = newStdGen >>= (\x -> return (makeLineSplits (intToListElement patternElementList) x)) >>= mapM_ (print . showAC)

lineSplitsToObjPs :: StdGen -> [[[PatternElement a]]] -> [ObjectPattern a]
lineSplitsToObjPs stdgen list = zipWith lineSplitToObject (map mkStdGen (randoms stdgen)) list

lineSplitToObject :: StdGen -> [[PatternElement a]] -> ObjectPattern a
lineSplitToObject stdgen m = addOffsetToPseudoObject (fst spl) (lineSplitToPseudoObject (snd spl) m)
    where spl = split stdgen

lineSplitToPseudoObject :: StdGen -> [[PatternElement a]] -> (Wildcard, [LinePattern a], Wildcard)
lineSplitToPseudoObject stdgen list = (fst (tokens !! 0), lines, snd (tokens !! 0))
    where tokens = sliceVertPair $ map (\x -> Null) (map (\x -> x `mod` 10) (randoms stdgen :: [Int]))
          lines = zipWith (\(a,b) x -> (a,x,b)) (drop 1 tokens) list

addOffsetToPseudoObject :: StdGen -> (Wildcard, [LinePattern a], Wildcard) -> ObjectPattern a
addOffsetToPseudoObject stdgen (w1, l, w2) = (w1, nl, w2)
    where nl = offsetLines stdgen l

offsetLines :: StdGen -> [LinePattern a] -> [(Int, LinePattern a)]
offsetLines stdgen m = offsetLines' [(0, head m)] stdgen (tail m)

offsetLines' :: [(Int, LinePattern a)] -> StdGen -> [LinePattern a] -> [(Int, LinePattern a)]
offsetLines' a stdgen ([]) = fixGeneratedObject a
offsetLines' a stdgen m = offsetLines' (newLine:a) (mkStdGen (bad !! 0)) (tail m)
    where bad = randoms stdgen 
          minSpot = (fst (head a)) - length (snd3 (head m)) + 1
          maxSpot = (fst (head a) + length (snd3 (snd(head a))))
          lineSpot = minSpot + (bad !! 1) `mod` (maxSpot - minSpot)
          newLine = (lineSpot, head m)

makeRecombinationPattern :: (Int -> PatternElement a) -> StdGen -> RecombinationRule a
makeRecombinationPattern f stdgen = (q, w, [], [], [])
    where bad = randoms stdgen
          list = generateList f (mkStdGen (bad !! 0))
          slist = shuffleList (mkStdGen (bad !! 1)) list
          q = lineSplitsToObjPs (mkStdGen (bad !! 2)) (makeLineSplits (mkStdGen (bad !! 3)) list)
          w = lineSplitsToObjPs (mkStdGen (bad !! 4)) (makeLineSplits (mkStdGen (bad !! 5)) slist)

fixGeneratedObject :: [(Int, a)]-> [(Int, a)]
fixGeneratedObject a = map (\(x,y) -> (x+offset, y)) a
    where offset = -1 * (minimum (map (\(x,y) -> x) a))


makeRecombinationRule :: (Eq a) => [PatternElement a] -> StdGen -> RecombinationRule a
makeRecombinationRule f stdgen = (fst5 base, snd5 base, w0, w1, w2)
    where spli = split stdgen
          base = makeRecombinationPattern (intToListElement f) (fst spli)
          result = sumObjectPatterns f (fst5 base)
          numWE = (snd4 result) !! 0
          numWL = trd4 result
          numWO = fth4 result
          bad = randoms (snd spli)
          s0 = mkStdGen (bad !! 0)
          s1 = mkStdGen (bad !! 1)
          s2 = mkStdGen (bad !! 2)
          w0 = unsort s0 [0..(numWE - 1)]
          w1 = unsort s1 [0..(numWL - 1)]
          w2 = unsort s2 [0..(numWO - 1)]

--only tested with no wildcards lol
makeRecombinationRuleConstrained :: (Eq a) => ([ObjectPattern a], [ObjectPattern a]) -> [PatternElement a] -> StdGen -> RecombinationRule a
makeRecombinationRuleConstrained (rc,pc) f stdgen = (rc++q, pc++w, [], [], [])
    where bad = randoms stdgen
          diff = determineDifference f (rc, pc)
          extra = foldl' (\x y -> x + sizeOfObjectPattern y) 0 (rc++pc)
          lengthToTake = minTokenLength + ((bad !! 6) `mod` (max (maxTokenLength - extra) 1)) --this probably should be made so it scales based on size of rc and pc
          list = take lengthToTake $ generateList (intToListElement f) (mkStdGen (bad !! 0))
          listf = list ++ snd diff
          slist = shuffleList (mkStdGen (bad !! 1)) list ++ fst diff
          q = lineSplitsToObjPs (mkStdGen (bad !! 2)) (makeLineSplits (mkStdGen (bad !! 3)) listf)
          w = lineSplitsToObjPs (mkStdGen (bad !! 4)) (makeLineSplits (mkStdGen (bad !! 5)) slist)


makeRecombinationRuleExact :: (Eq a) => ([ObjectPattern a], [ObjectPattern a]) -> [PatternElement a] -> StdGen -> RecombinationRule a
makeRecombinationRuleExact (rc, pc) f stdgen = (rc++q, pc++w, [], [], [])
    where bad = randoms stdgen
          diff = determineDifference f (rc, pc)
          extra = foldl' (\x y -> x + sizeOfObjectPattern y) 0 (rc++pc)
          listf = snd diff
          slist = fst diff
          q = lineSplitsToObjPs (mkStdGen (bad !! 2)) (makeLineSplits (mkStdGen (bad !! 3)) listf)
          w = lineSplitsToObjPs (mkStdGen (bad !! 4)) (makeLineSplits (mkStdGen (bad !! 5)) slist)

makeRecombinationRuleSingletons :: (Eq a) => ([ObjectPattern a], [ObjectPattern a]) -> [PatternElement a] -> StdGen -> RecombinationRule a
makeRecombinationRuleSingletons (rc, pc) f stdgen = (rc++q, pc++w, [], [], [])
    where diff = determineDifference f (rc, pc)
          extra = foldl' (\x y -> x + sizeOfObjectPattern y) 0 (rc++pc)
          listf = snd diff
          slist = fst diff
          q = map singletonToObjP listf
          w = map singletonToObjP slist

singletonToObjP :: (Eq a) => PatternElement a -> ObjectPattern a
singletonToObjP a = (Null, [(0, (Null, [a], Null))], Null)

determineDifference :: (Eq a) => [PatternElement a] -> ([ObjectPattern a], [ObjectPattern a]) -> ([PatternElement a], [PatternElement a])
determineDifference f (rc, pc) = createDiff f mList
    where retR = sumObjectPatterns f rc
          retP = sumObjectPatterns f pc
          mList = zipWith (-) (snd4 retR) (snd4 retP)

createDiff :: (Eq a) => [PatternElement a] -> [Int] -> ([PatternElement a], [PatternElement a])
createDiff = createDiff' ([], [])

createDiff' :: (Eq a) => ([PatternElement a], [PatternElement a]) -> [PatternElement a] -> [Int] -> ([PatternElement a], [PatternElement a])
createDiff' (a, b) f [] = (a,b)
createDiff' (a, b) (f:fs) (o:os) = 
    if o > -1
       then createDiff' ((take (abs o) (repeat f)) ++ a, b) fs os
       else createDiff' (a, (take (abs o) (repeat f))++b) fs os

--for now we aren't doing this function thanks to the wonderful exisistence of a computer which can very efficicnelty bash these out 
--takes about 3.6 seconds to generate 196 good rrs.
--the rate is high enough that I have no issue with just bashing it over and over again
--in about 5 minutes i got 20000 elements (check google drive, goodRRs)
--so yea very good enough
{-
ensureWildcardEquality :: RecombinationRule a -> RecombinationRule a
ensureWildcardEquality (r, p, _, _, _) = 
    where resultR = sumObjectPatterns basis r
          resultP = sumObjectPatterns basis p
          o = fth4 resultR - fth4 resultP
          l = trd4 resultR - trd4 resultP
          nr = 
          np
-}
testChanceOfPerfect :: IO()
testChanceOfPerfect = do 
    g <- newStdGen
    let m = randoms g
    let d = map (\x -> makeRecombinationPattern (intToListElement patternElementList) x) (map mkStdGen m)
    let ret = map (\x -> (conservationOfWildcardEvaluation patternElementList x)) (take 10000 d)
    print (length (filter (==1) ret))

generatePerfect :: String -> IO()
generatePerfect str = do 
    g <- newStdGen
    let m = randoms g
    let d = map (\x -> makeRecombinationRule (patternElementList) x) (map mkStdGen m)
    let ret = filter (\x -> (conservationOfWildcardEvaluation patternElementList x) == 1) (take 1000000 d)
    mapM_ (\x -> appendFile str (showAC x ++ "\n")) ret
    print $ length ret































