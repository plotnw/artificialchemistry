module Graphics.Render where

import           Control.Monad                  ( unless
                                                , when
                                                )
import           Graphics.Rendering.OpenGL
                                         hiding ( Front )
import           Graphics.Rendering.OpenGL.Capture
                                                ( capturePPM )
import qualified Graphics.UI.GLFW              as G
import           System.Exit
import           System.IO
import           Graphics.Rendering.FTGL
import qualified Data.ByteString.Char8         as B

import           System.Random

import           ArtificialChemistry.Data
                                         hiding ( main )
import           ArtificialChemistry.Examples
                                         hiding ( main )
import           ArtificialChemistry.Random
                                         hiding ( main )
import           Util

-- tiny utility functions, in the same spirit as 'maybe' or 'either'
-- makes the code a wee bit easier to read

bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x

-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description

keyCallback :: G.KeyCallback
keyCallback window key scancode action mods =
    when (key == G.Key'Escape && action == G.KeyState'Pressed)
        $ G.setWindowShouldClose window True

main :: IO ()
main = do
    g <- newStdGen
    let b = generateRandomObjectPattern (intToListElement patternElementList) g
    print $ showAC b
    --print $ getDimensionObject b
    G.setErrorCallback (Just errorCallback)
    successfulInit <- G.init
    -- if init failed, we exit the program
    bool successfulInit exitFailure $ do
        mw <- G.createWindow 640 640 "float" Nothing Nothing
        maybe' mw (G.terminate >> exitFailure) $ \window -> do
            G.makeContextCurrent mw
            clearColor $= Color4 1 1 1 1
            G.setKeyCallback window (Just keyCallback)
            mainLoop b window
            G.destroyWindow window
            G.terminate
            exitSuccess

globalFont :: IO Font
globalFont = createTextureFont
    "/home/weznon/programming/artificialChemistry/src/Graphics/cmuntt.ttf"

mainLoop :: (ShowAC a, Eq a) => ObjectPattern a -> G.Window -> IO ()
mainLoop o w = unless' (G.windowShouldClose w) $ do
    clear [ColorBuffer]
    loadIdentity
    lineWidth $= 1
    globalFont >>= (drawBox 0 0 "X")
    globalFont >>= (drawBox 1 0 "W")
    globalFont >>= (drawBox 2 0 "B")
    globalFont >>= (drawBox 2 1 "A")
    --globalFont >>= (drawObjectPattern 10 0 o)
    --globalFont >>= (drawLinePattern 10 0 linePatternNW_WA)
    --globalFont >>= drawObjectPattern 0 0 objectPattern1
    let x = capturePPM
    x >>= B.writeFile "/home/weznon/programming/artificialChemistry/images/image.ppm"
    G.swapBuffers w
    G.pollEvents
    mainLoop o w

boxDimension :: Double
boxDimension = 0.3

drawOpenBox :: Bool -> Double -> Double -> IO()
drawOpenBox _ _ _ = return ()
--drawOpenBox left = if left then drawOpenBoxLeft else drawOpenBoxRight
   
drawOpenBoxLeft :: Double -> Double -> IO()
drawOpenBoxLeft x' y' = do
    let x = boxDimension * x'
    let y = -(boxDimension * y')
    renderPrimitive LineStrip $ do
        color (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 (x + boxDimension/2 - 0.8) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension/2 + boxDimension/2 - 0.8 ) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension/2 + boxDimension/2 - 0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension/2 - 0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        color (Color3 0 0 1 :: Color3 GLdouble)
    loadIdentity  

drawOpenBoxRight :: Double -> Double -> IO()
drawOpenBoxRight x' y' = do
    let x = boxDimension * x'
    let y = -(boxDimension * y')
    renderPrimitive LineStrip $ do
        color (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 (x + boxDimension/2 - 0.8 ) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x - 0.8) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x - 0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension/2 - 0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        color (Color3 0 0 0 :: Color3 GLdouble)
    loadIdentity  

drawFullBox :: Double -> Double -> IO()
drawFullBox x' y' = do 
    let x = boxDimension * x'
    let y = -(boxDimension * y')
    loadIdentity
    renderPrimitive Polygon  $ do
        color (Color3 0 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (x-0.8) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension-0.8 ) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
    loadIdentity

drawBox :: Double -> Double -> String -> Font -> IO ()
drawBox x' y' w font = do
    let x = boxDimension * x'
    let y = -(boxDimension * y')
    translate $ (Vector3 (x + boxDimension / 4-0.778) (y + boxDimension / 4) 0 :: Vector3 GLdouble)
    scale 0.002 0.002 (0 :: GLfloat)
    setFontFaceSize font 100 10
    renderFont font w Front
    loadIdentity
    renderPrimitive LineLoop $ do
        color (Color3 0 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (x-0.8) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension-0.8 ) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + boxDimension-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
    loadIdentity

drawObject :: (ShowAC a, Eq a) => Double -> Double -> Object a -> Font -> IO ()
drawObject xOff yOff obj font = mapM_ (\(x, y) -> if (getElementAtIndexObject obj x y) == Nothing then return () else (drawBox (xOff + fromIntegral x) (yOff + fromIntegral y) (showAC (unsafeExtractMaybe (getElementAtIndexObject obj x y))) font)) yeet
  where
    dims = getDimensionObject obj
    yeet = [ (x, y) | x <- [0 .. (fst dims)], y <- [0 .. (snd dims)] ]

drawLinePattern :: (ShowAC a, Eq a) => Double -> Double -> LinePattern a -> Font -> IO()
drawLinePattern xOff yOff (w1, l, w2) f = do 
    if (w1 == Null) then (drawFullBox (xOff-1) yOff) else (drawOpenBox True (xOff-1) yOff)
    mapM_ (\(x,y) -> drawBox (xOff+y) yOff (showAC x) f) (zip l [0..])
    if (w2 == Null) then (drawFullBox (xOff+fromIntegral(length l)) yOff) else (drawOpenBox False (xOff+fromIntegral(length l)) yOff)

drawLineSequence :: Bool -> Int -> Double -> Double -> (Int, LinePattern a) -> IO()
drawLineSequence _ _ _ _ _ = return ()
--drawLineSequence True = drawLineSequenceTop
--drawLineSequence False = drawLineSequenceBot

drawLineSequenceBot :: Int -> Double -> Double -> (Int, LinePattern a) -> IO()
drawLineSequenceBot offset x' y' (off, (_, l, _))   = do
    let x = boxDimension * (x'+fromIntegral(off))
    let y = -(boxDimension * (y' + fromIntegral offset))
    loadIdentity
    renderPrimitive LineStrip $ do
        color (Color3 0 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (x-0.8) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + (fromIntegral (length (l)))*boxDimension-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + (fromIntegral (length (l)))*boxDimension-0.8 ) (y) 0 :: Vertex3 GLdouble)
        
    loadIdentity

drawLineSequenceTop :: Int -> Double -> Double -> (Int, LinePattern a) -> IO()
drawLineSequenceTop offset x' y' (off, (_, l, _))   = do
    let x = boxDimension * (x'+fromIntegral(off))
    let y = -(boxDimension * (y' - 1))
    loadIdentity
    renderPrimitive LineStrip $ do
        color (Color3 0 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (x + (fromIntegral (length (l)))*boxDimension-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        
        vertex (Vertex3 (x + (fromIntegral (length (l)))*boxDimension-0.8 ) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x-0.8) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
       
    loadIdentity

drawLineNull :: Bool -> Int -> Double -> Double -> (Int, LinePattern a) -> IO()
drawLineNull bool offset x' y' (off, (_, l, _))   = do
    let x = boxDimension * (x'+fromIntegral(off))
    let y = -(boxDimension * (y' + (if bool then (-1) else fromIntegral offset)))
    loadIdentity
    renderPrimitive Polygon  $ do
        color (Color3 0 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (x-0.8) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + (fromIntegral (length (l)))*boxDimension-0.8 ) (y) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x + (fromIntegral (length (l)))*boxDimension-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
        vertex (Vertex3 (x-0.8) (y + boxDimension) 0 :: Vertex3 GLdouble)
    loadIdentity



drawObjectPattern :: (ShowAC a, Eq a) => Double -> Double -> ObjectPattern a -> Font -> IO()
drawObjectPattern xOff yOff (w1, ls, w2) f = do
    if (w1 == Null) then drawLineNull True 0 xOff yOff (head ls) else drawLineSequence True 0 xOff yOff (head ls)
    mapM_ (\((off, x),y) -> drawLinePattern (xOff+fromIntegral off) (yOff+y) x f) (zip ls [0..])
    if (w2 == Null) then drawLineNull False (length ls) xOff yOff (last ls) else drawLineSequence False (length ls) xOff yOff (last ls)
    
