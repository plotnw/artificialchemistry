{-# LANGUAGE FlexibleInstances #-}
module SearchGenerator.ArtificialChemistry where

import ArtificialChemistry.Data hiding (main)
import SearchGenerator.Data hiding (main)
import System.Random
import System.IO.Unsafe
import ArtificialChemistry.Evaluate
import Debug.Trace
import Data.Array

--generateSimilarTokenGroups :: [a] -> [(PatternElement a, PatternElement a)]
--generateSimilarTokenGroups

instance SearchGeneratable ([RecombinationRule Token], Double) where
    evaluate = evaluateAC 
    combine = combineAC
    mutate = mutateAC
    create = createAC goodRRsSize goodRRs

goodRRs :: Array Int (RecombinationRule Token)
goodRRs = array (0,goodRRsSize) (zip [0..59045] (map readAC (lines (unsafePerformIO (readFile "/home/weznon/programming/artificialChemistry/src/goodRRs")))))

--hardcoded size, to alleviate some IO overhead
goodRRsSize :: Int
goodRRsSize = 43471

minRuleCount :: Int
minRuleCount = 200

maxRuleCount :: Int
maxRuleCount = 250

createAC :: Int -> (Array Int (RecombinationRule Token)) -> StdGen -> ([RecombinationRule Token], Double)
createAC length list stdgen = (listOfThings, compareTokensRules patternTokenList listOfThings)
    where bad = randoms stdgen
          howMany = minRuleCount + ((bad !! 0) `mod` (maxRuleCount - minRuleCount))
          is = take howMany (map (\x -> x `mod` length) (tail bad))
          listOfThings = map (\x -> list ! x) is




combineAC :: StdGen -> ([RecombinationRule Token], Double) -> ([RecombinationRule Token], Double) -> ([RecombinationRule Token], Double)
combineAC stdgen (t1,_) (t2,_) = (merp, compareTokensRules patternTokenList merp)
    where bad = randoms stdgen
          t1Take = 500 + ((bad !! 0) `mod` 250)
          t2Take = 500 + ((bad !! 1) `mod` 250)
          t1Taken = map (\x -> t1 !! x) (map (\x -> x `mod` (length t1)) (take t1Take (drop 2 bad)))
          t2Taken = map (\x -> t2 !! x) (map (\x -> x `mod` (length t2)) (take t1Take (drop (2+(length t1Taken)) bad)))
          merp = t1Taken ++ t2Taken

mutateAC :: StdGen -> ([RecombinationRule Token], Double) -> ([RecombinationRule Token], Double)
mutateAC _ l = l

evaluateAC :: ([RecombinationRule Token], Double) -> Double
evaluateAC (a,b) = b

runAC :: IO ()
runAC = do
    g <- newStdGen
    dab <- return $! (createSearchGenerator g :: SearchGenerator ([RecombinationRule Token], Double))
    testSearchGenerator "acTest.csv" 2 dab >>= print . findFittest . SearchGenerator.Data.pool
    --testSearchGenerator "acTest.csv" 1 dab >>= print

    --return dab >>= print . findFittest . SearchGenerator.Data.pool






















