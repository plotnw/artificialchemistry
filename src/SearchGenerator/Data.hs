module SearchGenerator.Data where

import System.Random
import Data.Sort
import Util hiding (combine)
import Debug.Trace
import Data.List

class SearchGeneratable a where
    evaluate :: a -> Double
    combine :: StdGen -> a -> a -> a
    mutate :: StdGen -> a -> a
    create :: StdGen -> a

--Example Implementation for Ints

instance SearchGeneratable Int where
    evaluate a = (fromIntegral a)/ 1000.0
    combine _ a b = (a+b) `div` 2
    mutate stdgen a = if (((fst (randomR (0.0,1.0) stdgen)) :: Double) > 0.9) then a + (fst (random stdgen)) `mod` 7 - 3 else a
    create stdgen = fst (random stdgen) `mod` 1000

testCreate :: IO()
testCreate = do
    g <- newStdGen
    f <- newStdGen
    let fig = create g :: Int
    print fig
    print $((mutate f fig) :: Int)


data SearchGenerator a = SearchGenerator {pool :: [a], gen :: StdGen, generation :: Int} deriving (Show)


poolSize :: Int
poolSize = 500

createSearchGenerator :: (SearchGeneratable a) => StdGen -> SearchGenerator a
createSearchGenerator stdgen = SearchGenerator mePool newgen 0
    where bad = randoms stdgen
          mePool = map create (take poolSize (map mkStdGen bad))
          newgen = mkStdGen (bad !! (poolSize + 1))

testcsg :: IO()
testcsg = do
    g <- newStdGen
    print (createSearchGenerator g :: SearchGenerator Int)

advanceSG :: (SearchGeneratable a) => SearchGenerator a -> SearchGenerator a
advanceSG = incrementSG . padSG . combineSG . mutateSG . cullSG

mutateSG :: (SearchGeneratable a) => SearchGenerator a -> SearchGenerator a
mutateSG sg = SearchGenerator newPool newgen (generation sg)
    where bad = randoms (gen sg)
          newPool = zipWith (\x y -> mutate (mkStdGen x) y) (tail bad) (pool sg)
          newgen = mkStdGen (head bad)

cullSG :: (SearchGeneratable a) => SearchGenerator a -> SearchGenerator a
cullSG sg = SearchGenerator cullPool newGen (generation sg)
    where bad = randoms (gen sg)
          newGen = mkStdGen (head bad)
          evalPool = map (\x -> (evaluate x, x)) (pool sg)
          cullBPool = drop (poolSize `div` 2) (sortBy (\(a,x) (b,y) -> compare a b) evalPool)
          cullPool = map (\(x,y) -> y) cullBPool

combineSG :: (SearchGeneratable a) => SearchGenerator a -> SearchGenerator a
combineSG sg = SearchGenerator newPool newGen (generation sg)
    where bad = randoms (gen sg)
          newGen = mkStdGen (head bad)
          range = length (pool sg)
          --iNeed = (poolSize - range)
          iNeed = 20
          dab0 = take iNeed (tail bad)
          dab1 = take iNeed (drop iNeed (tail bad))
          addPool = zipWith3 combine (map mkStdGen (drop (2*iNeed+1) bad)) (map (\x -> (pool sg) !! (x `mod` range)) dab0) (map (\x -> (pool sg) !! (x `mod` range)) dab1)
          newPool = addPool ++ (pool sg)

padSG :: (SearchGeneratable a) => SearchGenerator a -> SearchGenerator a
padSG sg = SearchGenerator newPool newGen (generation sg)
    where bad = randoms (gen sg)
          newGen = mkStdGen (head bad)
          iNeed = poolSize - (length (pool sg))
          newPool = (pool sg) ++ (take iNeed (map (\x -> create (mkStdGen x)) (tail bad)))

testIntGenerator :: IO()
testIntGenerator = do
    g <- newStdGen
    let dab = (createSearchGenerator g :: SearchGenerator Int)
    testSearchGenerator "intTest.csv" 1000 dab >>= print

testSearchGenerator :: (Show a, SearchGeneratable a) => String -> Int -> SearchGenerator a -> IO (SearchGenerator a)
testSearchGenerator filename generations sg = do
    let dab = iterate advanceSG sg
    --mapM_ ((\(SearchGenerator p g ge) -> appendFile filename (show (average (map evaluate p))) >> appendFile filename ",\n")) (take generations dab)
    -- ^ that line will recompute all of the evaluate steps, which are super slow for a chems
    return $ dab !! generations

incrementSG :: SearchGenerator a -> SearchGenerator a
incrementSG sg = trace (show (generation sg + 1)) (SearchGenerator (pool sg) (gen sg) (generation sg + 1))

findFittest :: (SearchGeneratable a) => [a] -> a
findFittest ([]) = error "empty list in SearchGenerator.Data.findFittest"
findFittest a = snd $ minimumBy (\(a,x) (b,y) -> a `compare` b) evalPool
    where evalPool = map (\x -> (evaluate x, x)) a
