{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module SearchGenerator.RecombinationRule where

import ArtificialChemistry.Data hiding (main)
import SearchGenerator.Data hiding (advanceSG)
import ArtificialChemistry.Random hiding (main)
import ArtificialChemistry.Evaluate
import Util hiding (combine)
import ArtificialChemistry.ACUtil
import ArtificialChemistry.RandomRR
import ArtificialChemistry.PatternMatching

import System.Random

import Data.List.Extra
import Debug.Trace

desiredObjects :: [Object Token]
desiredObjects = map readAC ["0#XZVW/3#XW/"
                            , "3#WYY/3#Y/1#ZZX/0#VV/"
                            , "0#XZXV/3#YYX/4#WV/3#XYWZ/"
                            , "2#VWW/0#WVZ/"
                            ,"0#WV/1#V/"
                            ,"1#ZYVX/0#VWZ/"
                            ,"2#VXXX/0#WXX/"
                            ,"2#VX/0#ZXWX/0#Y/0#V/"
                            ,"0#ZV/1#X/"
                            ]

instance SearchGeneratable (RecombinationRule Token) where
    evaluate = evaluateRR desiredObjects
    combine = combineRR
    mutate = mutateRR patternElementList
    create = makeRecombinationRule patternTokenListRestricted

combineRR :: StdGen -> RecombinationRule a -> RecombinationRule a -> RecombinationRule a
combineRR stdgen (r0, p0, _, _, _) (r1, p1, _, _, _) = (r, p, [], [], [])
    where bad = randoms stdgen
          howManyToTakeR = 1 + (minObjectRR + ((bad !! 1) `mod` maxObjectRR))
          howManyToTakeP = 1 + (minObjectRR + ((bad !! 0) `mod` maxObjectRR))
          takeR = generateWildcardMapping (mkStdGen (bad !! 2)) (length (r0++r1) - 1) howManyToTakeR
          takeP = generateWildcardMapping (mkStdGen (bad !! 3)) (length (p0++p1) - 1) howManyToTakeP
          r = map (\x -> ((r0 ++ r1) !! x)) takeR
          p = map (\x -> ((p0 ++ p1) !! x)) takeP

mutateRR :: (Eq a) => [PatternElement a] -> StdGen -> RecombinationRule a -> RecombinationRule a
mutateRR b stdgen a@(r, p, _, _, _) = a

testCombineAbility :: IO()
testCombineAbility = do
    f <- newStdGen
    g <- newStdGen
    h <- newStdGen
    let a = ((create f) :: RecombinationRule Token)
    let b = ((create g) :: RecombinationRule Token)
    let c = ((combine h a b) :: RecombinationRule Token)
    appendFile "combineRRTest.csv" (show (evaluate a))
    appendFile "combineRRTest.csv" ","
    appendFile "combineRRTest.csv" (show (evaluate b))
    appendFile "combineRRTest.csv" ","
    appendFile "combineRRTest.csv" (show (evaluate c))
    appendFile "combineRRTest.csv" ",\n"

evaluateRR :: (Eq a) => [Object a] -> RecombinationRule a -> Double
evaluateRR l (r, p, _, _, _) = result
    where max = length l
          temp = r++p
          merp = map (getMatches l) temp
          sum = foldl' (+) 0 (map (length) merp)
          result = fromIntegral(sum) / fromIntegral(length(temp))



-- General usable module that repeats any IO action n times.
-- by Niko Heikkilä
repeatIOAction :: Int -> IO () -> IO ()     -- Inputs: integer and IO. Outputs: IO 
repeatIOAction 0 _ = return ()              -- exit recursive loop here
repeatIOAction n action = do
    action                                  -- action to perform
    repeatIOAction (n-1) action             -- decrement n to make it recursive

-- Example: print a string "Hello World" 10 times to console
-- main = repeatIOAction 10 (putStrLn "Hello World")

testRRGenerator :: String -> IO()
testRRGenerator a = do
    g <- newStdGen
    let dab = (createSearchGenerator g :: SearchGenerator (RecombinationRule Token))
    let m = testACSearchGenerator a dab
    print "/n List of Unique ObjectPatterns \n --------------------"
    m >>= (\x -> (mapM_ (\(x,y) -> print (show y ++ showAC x)) (deduplicateSearchGenerator x)))


testACSearchGenerator :: (Show a, ShowAC a, SearchGeneratable a) => String -> SearchGenerator a -> IO (SearchGenerator a)
testACSearchGenerator filename sg = do
    let dab = iterate advanceSG sg
    mapM_ ((\(SearchGenerator p g ge) -> appendFile filename (show (average (map evaluate p))) >> appendFile filename ",\n")) (take 100 dab)
    --print $ dab !! 1000 
    mapM_ (\x -> print (showAC x)) ((\(SearchGenerator p g ge) -> p) (dab !! 100))
    return $ dab !! 100

lookAtMutation :: IO()
lookAtMutation = do
    g <- newStdGen
    let dab = makeM g
    let dabs = map rr $ take 10000 $ iterate advanceM dab
    mapM_ (\x -> print "---------" >> print (showAC x) >> print ( conservationOfMatterEvaluation patternElementList x) >> print (doTheThing x)) dabs
    
    
doTheThing :: RecombinationRule Token -> [Int]
doTheThing (r, p, _, _, _) = (zipWith (-) (snd4 (sumObjectPatterns patternElementList r)) (snd4 (sumObjectPatterns patternElementList p)))


data MM = MM {stdgen :: StdGen, rr :: RecombinationRule Token}

instance Show MM where
    show = showAC . rr

makeM :: StdGen -> MM
makeM stdgen = (MM newstdgen (create newstdgen2))
    where newstdgen = mkStdGen ((randoms stdgen) !! 0)
          newstdgen2 = mkStdGen ((randoms stdgen) !! 1)

advanceM :: MM -> MM
advanceM (MM stdgen rr) = (MM (snd yeet) (mutateRR patternElementList (mkStdGen (fst yeet)) rr))
    where yeet = random stdgen



deduplicateSearchGenerator :: SearchGenerator (RecombinationRule Token)-> [(ObjectPattern Token, Int)]
deduplicateSearchGenerator (SearchGenerator p g ge) = map (\x -> (x, length (filter (==x) un))) nu ++[ ((Null, [], Null), no)]
    where un = deduplicateRRPool [] (filter (\x -> conservationOfMatterEvaluation patternElementList x >= 1.0) p)
          no = length un
          nu = nub un

    
deduplicateRRPool :: (Eq a) => [ObjectPattern a] -> [RecombinationRule a] -> [ObjectPattern a]
deduplicateRRPool a ([]) = a
deduplicateRRPool a ((r, p, _, _, _):xs) = deduplicateRRPool (r ++ p ++ a) xs

advanceSG :: (SearchGeneratable a) => SearchGenerator a -> SearchGenerator a
advanceSG = incrementSG . padSG . cullSG






