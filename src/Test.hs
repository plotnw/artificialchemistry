module Test where

import ArtificialChemistry.Data hiding (main)
import ArtificialChemistry.Random hiding (main)
import ArtificialChemistry.PatternMatching
import System.Random
import Util

goodRRs :: IO [RecombinationRule Token]
goodRRs = readFile "goodRRs" >>= (\x -> return (map (\y -> readAC y :: RecombinationRule Token) (lines x)))

searchObject :: StdGen -> IO (Object Token)
searchObject stdgen = return $ generateRandomObject (intToListElement tokenList) stdgen

isIn :: Object Token -> RecombinationRule Token -> Bool
isIn obj rr = isIn' obj (r ++ p)
    where r = fst5 rr
          p = snd5 rr

isIn' :: Object Token -> [ObjectPattern Token] -> Bool
isIn' obj ([]) = False
isIn' obj (x:xs) = if (matchObject x obj /= []) then True else isIn' obj xs

main :: IO()
main = do
    let obj = newStdGen >>= searchObject
    obj >>= (\x -> print (showAC x))
    goodRRs >>= (\y -> (obj >>= (\z -> mapM_ (\x -> print (isIn z x)) y)))
