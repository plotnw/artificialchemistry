module Unsort where

import System.Random
import Data.List

unsort :: StdGen -> [a] -> [a]
unsort stdgen l = removeWeights . sortRandomWeights $ giveRandomWeights stdgen l

testUnsort :: IO [Int]
testUnsort = newStdGen >>= (\x -> return (unsort x [0,1,2,3,4,5,6,7,8,9]))


giveRandomWeights :: StdGen -> [a] -> [(Int, a)]
giveRandomWeights stdgen l = zip (randoms stdgen) l

sortRandomWeights :: [(Int, a)] -> [(Int, a)]
sortRandomWeights = sortBy (\(a,x) (b,y) -> compare a b)

removeWeights :: [(Int, a)] -> [a]
removeWeights = map (\(x,y) -> y)
