module Util where

import Data.List

safeGet :: [a] -> Int -> Maybe a
safeGet l i
    | i < 0 = Nothing
    | i >= (length l) = Nothing
    | otherwise = Just (l !! i)

unsafeExtractMaybe :: Maybe a -> a
unsafeExtractMaybe Nothing = error "this is so sad alexa play despacito"
unsafeExtractMaybe (Just a) = a

singletonString :: String -> [String]
singletonString ([]) = []
singletonString (x:xs) = [x]:(singletonString(xs))

combine :: (Num a) => [a] -> [a] -> [a]
combine = combine' []

combine' :: (Num a) => [a] -> [a] -> [a] -> [a]
combine' c ([]) a = c ++ a
combine' c a ([]) = c ++ a
combine' c (x:xs) (y:ys) = combine' (c ++ [x+y]) xs ys

dropTriple :: (a,b,c) -> (b,c)
dropTriple (a,b,c) = (b,c)

-- the range better be right

shuffle :: [a] -> [Int] -> [a]
shuffle = shuffle' []

shuffle' :: [a] -> [a] -> [Int] -> [a]
shuffle' a b ([]) = a
shuffle' a b (x:xs)= shuffle' (a ++ [b !! x]) b xs

fst3 :: (a,b,c) -> a
fst3 (a,b,c) = a

snd3 :: (a,b,c) -> b
snd3 (a,b,c) = b

trd3 :: (a,b,c) -> c
trd3 (a,b,c) = c

fst4 :: (a,b,c,d) -> a
fst4 (a,b,c,d) = a

snd4 :: (a,b,c,d) -> b
snd4 (a,b,c,d) = b

trd4 :: (a,b,c,d) -> c
trd4 (a,b,c,d) = c

fth4 :: (a,b,c,d) -> d
fth4 (a,b,c,d) = d

fst5 :: (a,b,c,d,e) -> a
fst5 (a,b,c,d,e) = a

snd5 :: (a,b,c,d,e) -> b
snd5 (a,b,c,d,e) = b

average :: [Double] -> Double
average ([]) = 0
average x = (foldl' (+) 0.0 x) / (fromIntegral (length x))

intToListElement :: [a] -> Int -> a
intToListElement a i = a !! (i `mod` (length a))

sliceVertPair :: [a] -> [(a,a)]
sliceVertPair (x0:x1:xs) = (x0,x1) : sliceVertPair xs
sliceVertPair [] = []
sliceVertPair _ = error "odd number of elements"

pairs :: [a] -> [(a, a)]
pairs l = [(x,y) | (x:ys) <- tails l, y <- ys]
